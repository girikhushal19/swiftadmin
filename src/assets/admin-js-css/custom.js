var base = $("meta[name='data-base']").attr("content");
var csrf = $('meta[name="csrf-token"]').attr('content');

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    {
        if( charCode == 46)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    return true;
}

$(function () {
    $('#expiry_date').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    
    $('#order_from').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#order_to').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    
    $('#add_from_to').daterangepicker({
        //autoUpdateInput: false,
         locale: {
            format: 'DD/MM/YYYY',
              defaultDate: null
        }
    });
    
    $('.timepicker').datetimepicker({
      format: 'HH:mm'
    })
    
    /* Add product page */
    $("#category_id").change(function(){
        var category_id = $(this).val();
        var restaurant_id = $("#merchant_id").val();
        if(restaurant_id == "" ||restaurant_id == null || category_id == ""|| category_id ==null)
        {
            //alert("ifff");
            $("#sub_cat_error").text("La sélection des restaurants et des catégories est indispensable");
        }else{
            //alert(restaurant_id);
            //alert(category_id);
            //alert("elseee");
            $("#sub_cat_error").text("");
        }
        $.ajax({
			headers: {
                'X-CSRF-TOKEN': csrf
            },
			url :  base+'/admin/getSubcategory',
			type: "POST",
			data: {ci:category_id,restaurant_id:restaurant_id},
			success:function(response)
			{
				$(".responseAjax").html(response);
			}
		});
    });
    /*********/
    /* Add product page */
    $("#merchant_id").change(function(){
        var restaurant_id = $(this).val();
        var category_id = $("#category_id").val();
        if(restaurant_id == "" ||restaurant_id == null || category_id == ""|| category_id ==null)
        {
            //alert("ifff");
            $("#sub_cat_error").text("La sélection des restaurants et des catégories est indispensable");
        }else{
            //alert(restaurant_id);
            //alert(category_id);
            //alert("elseee");
            $("#sub_cat_error").text("");
        }
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': csrf
            },
            url :  base+'/admin/getSubcategory',
            type: "POST",
            data: {ci:category_id,restaurant_id:restaurant_id},
            success:function(response)
            {
                $(".responseAjax").html(response);
            }
        });
    });
    /*********/
});



/* Add Restaurant */
$("input[type='radio'][name='shop_type']").change(function(){ 
    if( $(this).is(":checked") ){ // check if the radio is checked
        var val = parseInt($(this).val()); // retrieve the value
        
        if(val === 1){
            $('.restroMain').show();
            $('.restroType').hide();
        }else if(val === 2){
            $('.restroMain').hide();
            $('.restroType').show();
        }
    }
});

$("#outletType").change(function(){ 
    if( $(this).val() ){ // check if the radio is checked
        var val = parseInt($(this).val()); // retrieve the value
        
        if(val === 1){
            $('.foodType').show();
        }else if(val === 2){
            $('.foodType').hide();
        }
    }
});

/* Product image trash */
function delImg(i,name,n){
    if(i!=='' && name !== ''){
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': csrf
            },
            type:'POST',
            data:{'i':i,'n':name},
            url: base+'/admin/prodImgTrash',
            success:function(res){
                var response = JSON.parse(res);
                //console.log(response);
                //console.log(response.error);
                if(response.error){
                    //alert(response.errorMessage);
                    $("#image_delete_error").show();
                    $("#image_delete_error").text(response.errorMessage);
                }else{
                    $("#image_delete_error").hide();
                    $("#image_delete_error").text("");
                    var elem = document.getElementById(n);
                    elem.parentElement.removeChild(elem);
                }
            }
        });
    }else{
        alert('Quelque chose a mal tourné');
    }
}

/* Product image trash */
function delImg_restro(i,name,n){
    if(i!=='' && name !== ''){
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': csrf
            },
            type:'POST',
            data:{'i':i,'n':name},
            url: base+'/restro/prodImgTrash_restro',
            success:function(res){
                var response = JSON.parse(res);
                    //console.log(response);
                    //console.log(response.error);alert(response.error);
                if(response.error){
                    //console.log(response.errorMessage);
                    //alert(response.errorMessage);
                    $("#image_delete_error").show();
                    $("#image_delete_error").text(response.errorMessage);
                }else{
                    $("#image_delete_error").hide();
                    $("#image_delete_error").text("");
                    var elem = document.getElementById(n);
                    elem.parentElement.removeChild(elem);
                }
            }
        });
    }else{
        alert('something went wrong');
    }
}
/*********/

$('.prod_id').multiselect({
    includeSelectAllOption: false,
    selectAllName:false,
    numberDisplayed: 1,
    selectAllText:'Tous les produits',
    maxHeight:true,
    // buttonText: function(options) {
    //     if (options.length === 0) {
    //         return 'Aucun sélectionné';
    //     }
    //     else {
    //         var selected = '';
    //         options.each(function() {
    //             selected += $(this).text() + ', ';
    //         });
    //         return selected.substr(0, selected.length -2);
    //     }
    // }
});