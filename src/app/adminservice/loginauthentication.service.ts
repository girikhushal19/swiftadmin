import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginauthenticationService {

  token:any;user_type:any;loggedInDetail:any;user_id:any;
   
   base_url = "http://localhost:4202/";
  base_url_node_admin = "http://localhost:9003/api/";
  base_url_node = "http://localhost:9003/";
  baseapiurl= "http://localhost:9003/admin-panel/"
  baseapiurl2="http://localhost:9003/"


  // base_url = "https://administrateur.swift-fashion.com/";
  // base_url_node_admin = "https://mainserver.swift-fashion.com/api/";
  // base_url_node = "https://mainserver.swift-fashion.com/";
  // baseapiurl= "https://mainserver.swift-fashion.com/admin-panel/"
  // baseapiurl2="https://mainserver.swift-fashion.com/"

  constructor() { }

  userLogin()
  {
    this.token = localStorage.getItem("token");
    return this.token;
  }    
  
  userLoggedInType()
  {
    this.user_type = localStorage.getItem("user_type");
    return this.user_type;
  }
  userLoggedIn()
  {
    this.user_id = localStorage.getItem("_id");
    return this.user_id;
  }

}
