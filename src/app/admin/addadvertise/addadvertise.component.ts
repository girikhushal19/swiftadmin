import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginauthenticationService } from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AngularEditorConfig } from '@kolkov/angular-editor';
@Component({
  selector: 'app-addadvertise',
  templateUrl: './addadvertise.component.html',
  styleUrls: ['./addadvertise.component.css']
})
export class AddadvertiseComponent implements OnInit {
  base_url: any;base_url_two:string='';
  formtitle: any;apiResponse:any;
  vendors:any;
  allSeller:string [] = [];


  constructor(private http: HttpClient, private activeRoute: ActivatedRoute, private loginAuthObj: LoginauthenticationService,
    private router: Router, public dialog: MatDialog,) {
    this.base_url = loginAuthObj.baseapiurl
    this.base_url_two = loginAuthObj.baseapiurl2
    this.vendors = []
    this.http.get(this.base_url_two+"api/seller/getallsellerforfilter",{}).subscribe(res => {
      //console.log(res);
      this.apiResponse = res;
      this.vendors =this.apiResponse.data
    })
   
  }

  ngOnInit(): void {}

  htmlContent :any;
  htmlContentWithoutStyles:any;
  showHTML(){
    // const myElement = document.getElementById('htmlDiv')!.innerHTML;
    // console.log(myElement)
    this.htmlContentWithoutStyles = document.getElementById("htmlDiv")!.innerHTML;

  }
  config: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '15rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    customClasses: [
      {
        name: "quote",
        class: "quote",
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h1",
      },
    ]
  }

  form = new UntypedFormGroup({
    title: new UntypedFormControl('', [Validators.required]),
    notificationtype: new UntypedFormControl('email', [Validators.required]),
    usertype: new UntypedFormControl('seller', [Validators.required]),
    selecteduser: new UntypedFormControl('', [Validators.required]),
    message: new UntypedFormControl('', [Validators.required]),
  });
  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  get f(){
    return this.form.controls;
  }
  submit() {
    if(this.form.valid)
    {
      console.log("form submited");
      this.allSeller.push(this.form.value.selecteduser);
      let queryParam = {  
        "title":this.form.value.title,
        "notificationtype":"email",
        "usertype":"seller",
        "selecteduser":this.allSeller,
        "message":this.form.value.message
      };
      this.http.post(this.base_url + "makenewannouncements", queryParam).subscribe(res => {
        console.log(res);
        this.router.navigate(['advertisement'])
        this.dialog.closeAll()
        window.location.reload()
      })
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }

}
