import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllOrganizerEarningComponent } from './all-organizer-earning.component';

describe('AllOrganizerEarningComponent', () => {
  let component: AllOrganizerEarningComponent;
  let fixture: ComponentFixture<AllOrganizerEarningComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllOrganizerEarningComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllOrganizerEarningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
