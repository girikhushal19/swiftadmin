import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveSellerComponent } from './active-seller.component';

describe('ActiveSellerComponent', () => {
  let component: ActiveSellerComponent;
  let fixture: ComponentFixture<ActiveSellerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActiveSellerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ActiveSellerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
