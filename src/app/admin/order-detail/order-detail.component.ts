import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { ActivatedRoute,Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.css']
})
export class OrderDetailComponent implements OnInit {
  myjson:any=JSON;

  singleRecord:any;base_url = "";base_url_node = ""; 
  record:any;record2:any;base_url_two:string = '';
  token:any;user_type:any;apiResponse:any;edit_id:string='';
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService, private router: Router)
  {
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.baseapiurl;
    this.base_url_two = this.loginAuthObj.baseapiurl2;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }

    this.edit_id = this.actRoute.snapshot.params['id'];
    this._http.get(this.base_url_node+"getorderdetailbyorderid/"+this.edit_id).subscribe((response:any)=>{
      //console.log("response  -->>> "+ JSON.stringify(response));
      this.record = response.data;
      this.record2 = response.data[0].sellerinfo;
      //console.log("record  -->>> "+ JSON.stringify(this.record2));
      //console.log("deliveryaddress new  -->>> "+ this.record[0].sellerinfo[0].deliveryaddress );
    });
  }

  ngOnInit(): void {
  }

}
