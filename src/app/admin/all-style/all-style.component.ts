import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { FormGroup, FormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
@Component({
  selector: 'app-all-style',
  templateUrl: './all-style.component.html',
  styleUrls: ['./all-style.component.css']
})
export class AllStyleComponent implements OnInit {


  base_url = "";base_url_node = "";
  addModelsSubmit:any;getModel:any;allModelList:any;
  token:any;user_type:any;apiResponse:any;formValue:any;record:any;totalPageNumber:any;allColor:any;queryParam:any;numbers:any;allColorCount:any;apiStringify:any;updateColorStatusApi:any;queryParamNew:any;numofpage_0:any;
  selectedIndex: number;


  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService) { 

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }

    this.updateColorStatusApi = this.base_url_node+"updateStyleStatusApi";
    this.allColorCount = this.base_url_node+"allStyleCount";
    this.allColor = this.base_url_node+"allStyle";
    this.getallColor(0);
    this.selectedIndex = 0;
  }

  ngOnInit(): void {
    this._http.get(this.allColorCount ).subscribe((response:any)=>{
      //console.log("allColorCount"+ JSON.stringify(response));
      this.totalPageNumber = response.totalPageNumber;
      this.numbers = Array(this.totalPageNumber).fill(0).map((m,n)=>n);
      //console.log("this.numbers "+this.numbers);
    });
  }

  getallColor(numofpage=0){
    this.selectedIndex = numofpage;
    //console.log("numofpage"+numofpage);

    this.queryParam = {"numofpage":numofpage};
    this._http.post(this.allColor,this.queryParam).subscribe((response:any)=>{
      //console.log("allColor"+response);
      this.apiStringify = JSON.stringify(response);
      //console.log("result"+this.apiStringify);
      //console.log(this.apiStringify.error);
      this.record = response.record;
      //console.log("this record "+this.record);
    });
  }
  updateColorStatus(id=null,status:number)
  {
    /*console.log("hii");
    console.log(id);
    console.log(status);*/
    this.queryParamNew = {"id":id,"status":status};
    this._http.post(this.updateColorStatusApi,this.queryParamNew).subscribe((response:any)=>{ 
      //console.log(response);
      this.apiResponse = response;
      if(this.apiResponse.status == true)
      {
        this.numofpage_0 = 0;
        this.queryParam = {"numofpage":this.numofpage_0};
        //console.log(this.queryParam);
        this._http.post(this.allColor,this.queryParam).subscribe((response:any)=>{
          //console.log("allModel"+JSON.stringify(response));
          this.record = response.record;
          
          //console.log("totalPageNumber"+this.totalPageNumber);
          //console.log(this.numbers);
        });
      }
    });
  }
}
