import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
 
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-all-client',
  templateUrl: './all-client.component.html',
  styleUrls: ['./all-client.component.css']
})
export class AllClientComponent implements OnInit {

base_url = "";base_url_node = "";
addModelsSubmit:any;getModel:any;allModelList:any;
token:any;user_type:any;apiResponse:any;formValue:any;record:any;totalPageNumber:any;allAdminClientMerchant:any;queryParam:any;numbers:any;allAdminClientMerchantCount:any;apiStringify:any;deleteBanner:any;base_url_node_plain:any;myJson:any;
selectedIndex: number;updateCatStatusApi:any;user_id:any;

constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService) { 

  this.base_url = this.loginAuthObj.base_url;
  this.base_url_node = this.loginAuthObj.base_url_node_admin;
  this.base_url_node_plain = this.loginAuthObj.base_url_node;

  this.token = this.loginAuthObj.userLogin();
  this.user_type = this.loginAuthObj.userLoggedInType();
  
  this.user_id = this.actRoute.snapshot.params['id'];
  //console.log(this.token);
  console.log("this.user_id   "+this.user_id);
  if(this.token === "" || this.user_type !== "admin")
  {
    window.location.href = this.base_url;
  }

  this.allAdminClientMerchantCount = this.base_url_node+"allAdminClientMerchantCount";
  //console.log(this.allAdminClientMerchantCount);
  this.allAdminClientMerchant = this.base_url_node+"allAdminClientMerchant";
  this.updateCatStatusApi = this.base_url_node+"updateCatTypeStatusApi";
  this.getallBanner(0);
  this.selectedIndex = 0;
}

ngOnInit(): void {
  this.queryParam = {"user_id":this.user_id};
  this._http.post(this.allAdminClientMerchantCount,this.queryParam).subscribe((response:any)=>{
    //console.log("allAdminClientMerchantCount"+response);
    this.totalPageNumber = response.totalPageNumber;
    this.numbers = Array(this.totalPageNumber).fill(0).map((m,n)=>n);
    //console.log("this.numbers "+this.numbers);
  });
}
getallBanner(numofpage=0){
  this.selectedIndex = numofpage;
  //console.log("numofpage"+numofpage);

  this.queryParam = {"numofpage":numofpage,"user_id":this.user_id};
  this._http.post(this.allAdminClientMerchant,this.queryParam).subscribe((response:any)=>{
    //console.log("allAdminClientMerchant"+response);
    this.apiStringify = JSON.stringify(response);
    //console.log("result"+this.apiStringify);
    //console.log(this.apiStringify.error);
    this.record = response.record;
    //console.log("this record "+this.record);
  });
}

  updateCatStatus(id=null,status:number)
  {
    this.queryParam = {"id":id,"status":status};
    this._http.post(this.updateCatStatusApi,this.queryParam).subscribe((response:any)=>{ 
      //console.log(response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.getallBanner(0);
      }
    });
  }
  form = new UntypedFormGroup({
    full_name: new UntypedFormControl('', []),
    user_id: new UntypedFormControl('', []),
    email: new UntypedFormControl('', []),
    phone: new UntypedFormControl('', []),
    city: new UntypedFormControl('', []), 
    status: new UntypedFormControl('', []),
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  submit()
  {
    console.log(this.form.value);
    this._http.post(this.allAdminClientMerchant,this.form.value).subscribe((response:any)=>{
      //console.log("allMerchant"+response);
      this.apiStringify = JSON.stringify(response);
      //console.log("result"+this.apiStringify);
      //console.log(this.apiStringify.error);
      this.record = response.record;
      //console.log("this record "+this.record);
    });
  }

}

