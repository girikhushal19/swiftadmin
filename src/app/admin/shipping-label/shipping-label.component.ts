import { Component, OnInit } from '@angular/core'; 
import {PrintService} from '../print.service';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { ActivatedRoute,Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-shipping-label',
  templateUrl: './shipping-label.component.html',
  styleUrls: ['./shipping-label.component.css']
})
export class ShippingLabelComponent implements OnInit {
  invoiceIds: string[] = [];
  invoiceDetails: Promise<any>[] = [];
  order_Number:any
  elementType = 'svg';
  value = '1234 0987 3455 3434';
  format = 'CODE128';
  lineColor = '#000000';
  width = 1.4;
  height = 50;
  displayValue = true;
  fontOptions = '';
  font = 'monospace';
  textAlign = 'center';
  textPosition = 'bottom';
  textMargin = 2;
  fontSize = 20;
  background = '#ffffff';
  margin = 10;
  marginTop = 10;
  marginBottom = 10;
  marginLeft = 10;
  marginRight = 10;

  get values(): string[] {
    return this.value.split('\n');
  }
  codeList: string[] = [
    '', 'CODE128',
    'CODE128A', 'CODE128B', 'CODE128C',
    'UPC', 'EAN8', 'EAN5', 'EAN2',
    'CODE39',
    'ITF14',
    'MSI', 'MSI10', 'MSI11', 'MSI1010', 'MSI1110',
    'pharmacode',
    'codabar'
  ];
  singleRecord:any;base_url = "";base_url_node = ""; 
  record:any;record2:any;base_url_two:string = '';
  token:any;user_type:any;apiResponse:any;edit_id:string='';
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService, private router: Router)
  {
    
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.baseapiurl;
    this.base_url_two = this.loginAuthObj.baseapiurl2;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }

    this.edit_id = this.actRoute.snapshot.params['id'];
    this._http.get(this.base_url_node+"getorderdetailbyorderid/"+this.edit_id).subscribe((response:any)=>{
      console.log("response  -->>> "+ JSON.stringify(response));
      this.record = response.data;
      this.record2 = response.data[0].sellerinfo;
      console.log("record2  -->>> "+ JSON.stringify(this.record2));
    });
  }

  ngOnInit(): void {
    console.log(this.actRoute.snapshot.params['id'])
    this.order_Number = this.actRoute.snapshot.params['id']
  }

 

  printPage(){
    window.print()
  }

}
