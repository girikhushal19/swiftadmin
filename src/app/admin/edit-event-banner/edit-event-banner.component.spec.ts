import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditEventBannerComponent } from './edit-event-banner.component';

describe('EditEventBannerComponent', () => {
  let component: EditEventBannerComponent;
  let fixture: ComponentFixture<EditEventBannerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditEventBannerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditEventBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
