import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllDeletedProductComponent } from './all-deleted-product.component';

describe('AllDeletedProductComponent', () => {
  let component: AllDeletedProductComponent;
  let fixture: ComponentFixture<AllDeletedProductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllDeletedProductComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllDeletedProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
