import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SendPushNotificationMerchantComponent } from './send-push-notification-merchant.component';

describe('SendPushNotificationMerchantComponent', () => {
  let component: SendPushNotificationMerchantComponent;
  let fixture: ComponentFixture<SendPushNotificationMerchantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SendPushNotificationMerchantComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SendPushNotificationMerchantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
