import { Component, OnInit ,ViewChild, ElementRef} from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { Editor } from 'ngx-editor';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { IDropdownSettings } from 'ng-multiselect-dropdown';


@Component({
  selector: 'app-send-push-notification-merchant',
  templateUrl: './send-push-notification-merchant.component.html',
  styleUrls: ['./send-push-notification-merchant.component.css']
})
export class SendPushNotificationMerchantComponent implements OnInit {
  editor: Editor;
  html: any;
  dropdownList : any[] = [];
  dropdownListNew : any[] = [];
  selectedItems : any[] = [];
   
     dropdownSettings = {};



  imageSrc: string = '';
  fileInputLabel: string = "";
  base_url = "";base_url_node = ""; 
  token:any;formData:any;images:any;video:any;user_type:any;apiResponse:any;formValue:any;sendMerchantPushNotificationSubmit:any;getModel:any;allModelList:any;getCarCategory:any;allCarCategory:any;getAllActiveUserApi:any;queryParam:any;record:any;
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.sendMerchantPushNotificationSubmit = this.base_url_node+"sendMerchantPushNotificationSubmit";
    this.getAllActiveUserApi = this.base_url_node+"getAllActiveMerchantAdminApi";
    //this.getCarCategory = this.base_url_node+"getCarCategory";
    //console.log("here");
    this.queryParam = {};
    this._http.post(this.getAllActiveUserApi,this.queryParam).subscribe((response:any)=>{
      //console.log("response of api getAllActiveUserApi"+response);
      //this.apiResponse = response;
      if(response.error == false)
      {
        //console.log(this.apiResponse);
        this.record = response.record;
        var abc = [];
        for (var _i = 0; _i < this.record.length; _i++)
        {
          abc.push({item_id: this.record[_i]._id,item_text: this.record[_i].first_name+" "+this.record[_i].last_name+" -- "+this.record[_i].phone});
          this.dropdownListNew = abc;
          //console.log(this.dropdownListNew);
          //this.dropdownListNew[_i] = { item_id: this.record[_i]._id, item_text: this.record[_i].firstName+" "+this.record[_i].lastName };
        }
      }
    });

    this.editor = new Editor();
    this.html = ""; 
    /*this.dropdownList = [
      { item_id: 1, item_text: 'Mumbai' },
      { item_id: 2, item_text: 'Bangaluru' },
      { item_id: 3, item_text: 'Pune' },
      { item_id: 4, item_text: 'Navsari' },
      { item_id: 5, item_text: 'New Delhi' }
    ];*/
    /*console.log("this.dropdownList");
    console.log(this.dropdownList);
    console.log("this.dropdownListNew");
    console.log(this.dropdownListNew);*/
    this.selectedItems = [ 
    ];
     


  }

  onItemSelect(item: any) {
    //console.log(item);
  }
  onSelectAll(items: any) {
    //console.log(items);
  }


  ngOnInit(): void {
         this.dropdownSettings = {
        singleSelection: false,
        idField: 'item_id',
        textField: 'item_text',
        selectAllText: 'Sélectionner tout',
        unSelectAllText: 'Ne pas sélectionner tout',
        itemsShowLimit: 10,
        allowSeachFilter: true
     } 
    this.editor = new Editor();
  }
    ngOnDestroy(): void
    {
      this.editor.destroy();
    }

  form = new UntypedFormGroup({
    all_user: new UntypedFormControl('', [Validators.required]),
    title: new UntypedFormControl('', [Validators.required]),
    description: new UntypedFormControl('', [Validators.required]),
    //fileSource: new FormControl('', [Validators.required])
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  

  submit()
  {
    //console.log(this.form.value);   
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this.formValue = this.form.value;
      
      /*this.formData.append('model_name', this.form.value.model_name);
      this.formData.append('parent_id', this.form.value.parent_id);
      this.formData.append('attribute_type', this.form.value.attribute_type);*/
      //formData.append('file', this.images);
      /*this.formValue = this.form.value;
      this.formValue = this.images;*/
      //console.log(this.form.value);
      ///console.log(this.form.value.model_name);
      this._http.post(this.sendMerchantPushNotificationSubmit,this.formValue).subscribe((response:any)=>{
          console.log("response of api"+response);
          this.apiResponse = response;
          if(this.apiResponse.error == false)
          {
            this.form.reset();
          }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}
