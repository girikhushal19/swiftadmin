import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MakePaymentRequestComponent } from './make-payment-request.component';

describe('MakePaymentRequestComponent', () => {
  let component: MakePaymentRequestComponent;
  let fixture: ComponentFixture<MakePaymentRequestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MakePaymentRequestComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MakePaymentRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
