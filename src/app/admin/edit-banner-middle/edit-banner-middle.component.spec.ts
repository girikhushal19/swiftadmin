import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditBannerMiddleComponent } from './edit-banner-middle.component';

describe('EditBannerMiddleComponent', () => {
  let component: EditBannerMiddleComponent;
  let fixture: ComponentFixture<EditBannerMiddleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditBannerMiddleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditBannerMiddleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
