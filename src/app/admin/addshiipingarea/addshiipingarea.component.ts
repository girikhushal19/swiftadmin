import { Component, OnInit,HostListener } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
@Component({
  selector: 'app-addshiipingarea',
  templateUrl: './addshiipingarea.component.html',
  styleUrls: ['./addshiipingarea.component.css']
})
export class AddshiipingareaComponent implements OnInit {
  @HostListener('paste', ['$event']) blockPaste(e: KeyboardEvent) {
    e.preventDefault();
  }

  @HostListener('copy', ['$event']) blockCopy(e: KeyboardEvent) {
    e.preventDefault();
  }

  @HostListener('cut', ['$event']) blockCut(e: KeyboardEvent) {
    e.preventDefault();
  }

  addNewZone:string='';city_array:any[] = [];geAlltZones:string='';
  base_url = "";base_url_two = ""; base_url_node_only = ""; base_url_live = "";
  getAllZoneRecord:any;getMulticity:any;
  constructor(public dialog: MatDialog,private _http:HttpClient,private loginAuthObj:LoginauthenticationService) {

    this.base_url = loginAuthObj.baseapiurl;
    this.base_url_two = loginAuthObj.baseapiurl2;
    this.addNewZone = this.base_url_two+"api/shipping/addNewZone";
    this.geAlltZones = this.base_url_two+"api/shipping/geAlltZones";
    this._http.get(this.geAlltZones,{}).subscribe((response:any)=>{
      console.log("dfdsf"+ JSON.stringify(response));
      if(response.status == true)
      {
        this.getAllZoneRecord = response.data;
      }
    });
   }
  city_name: string = '';apiResponse:any;
  address: string = '';latitude: string = '';longitude: string = '';
  displayStyle = "block";
  displayStyle2 = "none";
  ngOnInit(): void {
  }
  handleAddressChange(address: any,fun_city_name:'') {
    console.log(address);
    if(address)
    {
      if(address.address_components)
      {
        address.address_components.forEach(function (value:any) {
          if(value.types)
          {
            if(value.types.length > 0)
            {
              if(value.types[0] == 'locality')
              {
                // console.log("long_name "+value.long_name);
                // console.log("types "+value.types);
                fun_city_name = value.long_name;
              }
            }
          }
        }); 

      }
    }
    this.city_name = fun_city_name;
    //console.log("this.city_name  -->>  "+this.city_name);
    // console.log(address.address_components[0]);
    // console.log(address.address_components[1]);
    this.address = address.formatted_address
    this.latitude = address.geometry.location.lat()
    this.longitude = address.geometry.location.lng()
  }
  form = new UntypedFormGroup({
    zone_type: new UntypedFormControl('', [Validators.required]),
    zone_name: new UntypedFormControl('', []),
    zone_id: new UntypedFormControl('', []),
    address: new UntypedFormControl('', [Validators.required]),
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  onChangeR(event:any)
  {
    console.log("zone id "+event);

  }

  onChangeR2(event:any)
  {
    console.log("value "+event);
    if(event == 'new_zone')
    {
      this.displayStyle = "block";
      this.displayStyle2 = "none";
    }else{
      this.displayStyle = "none";
      this.displayStyle2 = "block";
    }
  }
  submit() {
    //this.dialog.closeAll()
    console.log("hereeeee");
    if(this.form.valid)
    {
      if(this.city_name == "" || this.city_name == null  || this.city_name == 'null' )
      {
        this.apiResponse = {"error":true,"errorMessage":"Avec cette adresse, nous n'avons pas trouvé de ville, veuillez entrer une autre adresse."};
      }if(this.form.value.zone_name == "" && this.form.value.zone_id == "")
      {
        this.apiResponse = {"error":true,"errorMessage":"Le nom de la zone est requis."};
      }else{
        if(this.form.value.zone_id)
        {
          //console.log("here ifffffff 133");
          //console.log("this.form.value.zone_id "+this.form.value.zone_id);
          this._http.get(this.base_url_two+"api/shipping/getZonesByZoneId/"+this.form.value.zone_id).subscribe((response:any)=>{
            //console.log("dfdsf"+ JSON.stringify(response));
            if(response.status == true)
            {
              this.getMulticity = response.data.cities;
              console.log("this.getMulticity line no 140"+JSON.stringify(this.getMulticity));

              let obj = {"city":this.city_name};
              this.getMulticity.push(obj);
              let getqueryParam = {"cities":this.getMulticity,"name":this.form.value.zone_name,"id":this.form.value.zone_id,"single_city":this.city_name};
              console.log("getqueryParam");
              console.log(getqueryParam);
              this._http.post(this.addNewZone,getqueryParam).subscribe((response:any)=>{
                console.log(response);
                this.apiResponse = response;
                if(response.status == true)
                {
                  setTimeout(() => {
                      window.location.reload();
                  }, 2000); 
                }

              });
            }
          });

          // console.log("getMulticity line no 144");
          // console.log(this.getMulticity);

          
        }else{
          console.log("here elseeee 158");
          let obj = {"city":this.city_name};
          this.city_array.push(obj);
          console.log("this.city_array "+JSON.stringify(this.city_array));
          let getqueryParam = {"cities":this.city_array,"name":this.form.value.zone_name,"id":''};
          this._http.post(this.addNewZone,getqueryParam).subscribe((response:any)=>{
            console.log(response);
            this.apiResponse = response;
            if(response.status == true)
            {
              setTimeout(() => {
                  window.location.reload();
              }, 2000); 
            }

          });
        }
        
      }
    }else{
      console.log("invalid form");
      this.validateAllFormFields(this.form); 
    }

  }

}
