import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetSellerTimeScheduleComponent } from './get-seller-time-schedule.component';

describe('GetSellerTimeScheduleComponent', () => {
  let component: GetSellerTimeScheduleComponent;
  let fixture: ComponentFixture<GetSellerTimeScheduleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetSellerTimeScheduleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GetSellerTimeScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
