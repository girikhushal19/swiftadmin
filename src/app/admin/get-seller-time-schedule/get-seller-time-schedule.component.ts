import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-get-seller-time-schedule',
  templateUrl: './get-seller-time-schedule.component.html',
  styleUrls: ['./get-seller-time-schedule.component.css']
})
export class GetSellerTimeScheduleComponent implements OnInit {
  searchText:any;
  base_url:any;
  apiResponse:any;base_url_two:string="";apiResponse_2:any;
  record:any;deletemultipleseller:string="";
  invoice_id:any[] = [];id:any;
   base_url_node = "";
  addModelsSubmit:any;getModel:any;allModelList:any;
  token:any;user_type:any; formValue:any; totalPageNumber:any;allRoutePlan:any;queryParam:any;numbers:any;allRoutePlanCount:any;apiStringify:any;updateRoutePlanStatusApi:any;base_url_node_plain:any;myJson:any;exportCsvDriverApi:any;
  selectedIndex: number;deleteRoutePlan:any;
  constructor(private loginAuthObj:LoginauthenticationService,private http: HttpClient,private actRoute: ActivatedRoute) {
    this.base_url = loginAuthObj.baseapiurl
    this.record = []
    this.base_url_two = loginAuthObj.baseapiurl2;
    
    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }

    //this.allRoutePlanCount = this.base_url_two+"api/seller/getCountPaymentRequestVendor";
    this.allRoutePlan = this.base_url_two+"api/seller/getTimeSloatSeller/";

    this.id = this.actRoute.snapshot.params['id'];

    this.queryParam = {"seller_id":this.id};
    this.http.get(this.allRoutePlan+this.id).subscribe((response:any)=>{
      //console.log("allRoutePlanCount"+response);
      this.apiResponse = response;
      this.record = this.apiResponse.record;
      
      //console.log("this.numbers "+this.numbers);
    });
 
    this.selectedIndex = 0;
   }


  ngOnInit(): void { }


   
   
}
