import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';

@Component({
  selector: 'app-all-market-place',
  templateUrl: './all-market-place.component.html',
  styleUrls: ['./all-market-place.component.css']
})
export class AllMarketPlaceComponent implements OnInit {
  searchText:any;
  select_all = false;
  usersarray:any;
  users:any
  orders:any;
  base_url:any;apiResponse:any;
  constructor(private http: HttpClient,private loginAuthObj:LoginauthenticationService) { 
    this.users =[]
    this.usersarray = []
    this.orders = []
    this.base_url = loginAuthObj.baseapiurl2
  }

  ngOnInit(): void {
    this.http.get(this.base_url+"api/shipping/getMshippingMethod").subscribe(res => {
       this.apiResponse = res;
      //console.log("sdfdsfdsf"+ JSON.stringify(res));
      this.orders = this.apiResponse.data;
    })
  }

  data: any[] = []


}
