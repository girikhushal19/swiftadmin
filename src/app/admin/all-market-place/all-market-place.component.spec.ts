import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllMarketPlaceComponent } from './all-market-place.component';

describe('AllMarketPlaceComponent', () => {
  let component: AllMarketPlaceComponent;
  let fixture: ComponentFixture<AllMarketPlaceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllMarketPlaceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllMarketPlaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
