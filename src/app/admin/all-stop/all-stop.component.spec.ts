import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllStopComponent } from './all-stop.component';

describe('AllStopComponent', () => {
  let component: AllStopComponent;
  let fixture: ComponentFixture<AllStopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllStopComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllStopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
