import { Component, OnInit, ViewChild, ElementRef,AfterViewInit } from '@angular/core';
import { GoogleMap, MapInfoWindow, MapMarker } from '@angular/google-maps';
import {MapDirectionsService} from '@angular/google-maps';
import {Observable} from 'rxjs';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
 

import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { Router,NavigationStart, Event as NavigationEvent } from '@angular/router';
import { UntypedFormGroup, UntypedFormControl, Validators,FormArray, FormBuilder,FormGroup} from '@angular/forms';
@Component({
  selector: 'app-assign-route-to-driver',
  templateUrl: './assign-route-to-driver.component.html',
  styleUrls: ['./assign-route-to-driver.component.css']
})
export class AssignRouteToDriverComponent implements OnInit {
  
  markers:any[] = [];
  position:any;
  @ViewChild('map', {static: false}) mapElement:any= ElementRef;
  map:any;
  marker:any;
  directionsRendererccc:any;
  center = new google.maps.LatLng(48.8592877,2.3310465);
  html_view:string="";
  logged_in_user_id:any;
  queryParam:any;userPermissionRecord:any;base_url:any;base_url_node:any;base_url_node_plain:any;
  record:any;apiResponse:any;
  getStopListWithDriver:any;getqueryParam:any;
  totalDistance:number=0;totalTimeInMinute:number=0;totalTimeInHourMinute:number=0;hour:number=0;
  totalHour:number=0;totalMin:number=0;route_id:any;driver_id:any;currentRoute:any;urlLength:number=0;currentUrlSegment:any;assignToDriverApi:string='';
  routeRecord:any;driverRecord:any;newObject:object={};startEndRoute:any;
  updatePositionRoute:any;updatePositionRouteLast:any;updateTotalTimeDistance:any;finalRouteAssignApi:any;finalRouteAssignApi2:any;
  // mapOptions: google.maps.MapOptions = {
  //   center: this.center,
  //   zoom: 10,
  //   mapTypeId: google.maps.MapTypeId.ROADMAP
  // };
  
  productForm: UntypedFormGroup; dd:any;mm:any;
   

  constructor( private fb:FormBuilder,private router: Router,private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService) {
    
    this.currentRoute = this.router.url.split('/');
    //console.log(this.currentRoute);
    //console.log(this.currentRoute.length);
    if(this.currentRoute.length == 4)
    {
      //console.log("here");
      this.urlLength = this.currentRoute.length-2;
      this.route_id = this.currentRoute[this.urlLength];

      this.urlLength = this.currentRoute.length-1;
      this.driver_id = this.currentRoute[this.urlLength];

      //console.log("here 320 route_id---->"+this.route_id);
      //console.log("here 320 driver_id---->"+this.driver_id);
    }

    //console.log(this.route_id);
    //console.log(this.driver_id);
    this.map= google.maps.Map;

    this.base_url = this.loginAuthObj.base_url;
    this.logged_in_user_id = localStorage.getItem("_id");
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.assignToDriverApi = this.base_url_node+"assignToDriverApi";
    this.updatePositionRoute = this.base_url_node+"updatePositionRoute";
    this.updatePositionRouteLast = this.base_url_node+"updatePositionRouteLast";
    this.updateTotalTimeDistance = this.base_url_node+"updateTotalTimeDistance";
    
    this.finalRouteAssignApi = this.base_url_node+"finalRouteAssignApi";
    this.finalRouteAssignApi2 = this.base_url_node+"finalRouteAssignApi2";
    marker: google.maps.Marker;

    
    this.productForm = this.fb.group({
      route_id: new UntypedFormControl('', [ ]),
      driver_id: new UntypedFormControl('', [ ]),
      start_date: new UntypedFormControl('', [Validators.required]),
      start_time: new UntypedFormControl('', [Validators.required]),
    });

  }
  ngOnInit() {
    //console.log(this.markers);
    //console.log(this.markers[0].latitude);
  }
  ngAfterViewInit(){

    this.mapInit();
    const directionsService = new google.maps.DirectionsService();
    const directionsRenderer = new google.maps.DirectionsRenderer();
    
    this.getqueryParam = {"route_id":this.route_id,"driver_id":this.driver_id};
    this._http.post(this.finalRouteAssignApi,this.getqueryParam).subscribe((response:any)=>{
      this.getqueryParam = {"route_id":this.route_id,"driver_id":this.driver_id};
      this._http.post(this.finalRouteAssignApi2,this.getqueryParam).subscribe((response:any)=>{
          
          this.getqueryParam = {"route_id":this.route_id,"driver_id":this.driver_id};
          this.getStopListWithDriver = this.base_url_node+"getStopListWithDriver"; 
          this._http.post(this.getStopListWithDriver,this.getqueryParam).subscribe((response:any)=>{
            
            if(response.error == false)
            {
              //console.log("api response "+ JSON.stringify(response));
              this.startEndRoute = response.startEndRoute;
              //console.log("api response "+ JSON.stringify(this.driverRecord));
              this.routeRecord = response.routeRecord;
              //console.log("this.routeRecord.length "+this.routeRecord.length);
              //console.log("this.routeRecord "+JSON.stringify(this.routeRecord));
              for(let x=0; x<this.routeRecord.length; x++)
              {
                //console.log("id  --> "+this.routeRecord[x]._id);
                this.newObject = {};
                
                  
                  var html = "";
                  html += '<div class="stop-info-dialogue ui-widget-content ui-draggable ui-draggable-handle" id="modal-stop-detail" style="display: block;">';
                  html += '<div class="stop-info-header d-flex justify-content-between align-items-cemnter" style="background-color: rgb(0, 0, 0);">';
                    html += '<h6 class="stop_info_header">Stop No - 2 (Sadik)</h6>';
                    html += '<ul class="d-flex align-items-center">';

                      // html += '<li><a href="javascript:editStopReviewRoute(1798995);" title="Edit" class="editStop"><img src="https://cdncrew.upperinc.com/public/assets/img/stop-edit.svg" class="img-fluid" alt="Edit"></a></li>';
                      // html += '<li><a href="javascript:beforeDelete(1798995);" title="Delete" class="deleteStop"><img src="https://cdncrew.upperinc.com/public/assets/img/delete-icon.svg" class="img-fluid" alt="Delete"></a></li>';

                      html += '<li><a href="#" title="Close" class="close-icon"><img src="https://cdncrew.upperinc.com/public/assets/img/stop-close.svg" class="img-fluid" alt="Close"></a></li>';
                    html += '</ul>';
                    html += '</div>';
                    html += '<div class="stop-info-body">';
                    html += '<ul class="d-flex flex-wrap">';
                      html += '<li  style="list-style-type: none; padding-top: 5px; display: block; width: 100%;" >';
                          html += '<label>Adresse</label>';
                          html += '<p class="" id="view_stop_address" data-toggle="tooltip" data-placement="bottom" title="New Delhi, Delhi, India" data-original-title="">'+this.routeRecord[x].address+'</p>';
                      html += '</li>';
                      html += '<li  style="list-style-type: none; padding-top: 5px; display: block; width: 100%;" >';
                          html += '<label>Heure</label>';
                          html += '<p class="" id="view_stop_arrived_time">07:50 AM</p>';
                      html += '</li>';
                      html += '<li  style="list-style-type: none; padding-top: 5px; display: block; width: 100%;" >';
                          html += '<label>Nom du contact</label>';
                          html += '<p class="" id="view_stop_v_nick_name">'+this.routeRecord[x].fullName+'</p>';
                      html += '</li>';
                      
                      html += '<li  style="list-style-type: none; padding-top: 5px; display: block; width: 100%;" >';
                          html += '<label>Nom de l entreprise</label>';
                          html += '<p class="" id="view_stop_company_name">'+this.routeRecord[x].businessName+'</p>';
                      html += '</li>';
                      html += '<li  style="list-style-type: none; padding-top: 5px; display: block; width: 100%;" >';
                          html += '<label>Téléphone</label>';
                          html += '<p class="" id="view_stop_phone">'+this.routeRecord[x].mobileNumber+'</p>';
                      html += '</li>';
                      html += '<li  style="list-style-type: none; padding-top: 5px; display: block; width: 100%;" >';
                          html += '<label>Email</label>';
                          html += '<p class="" id="view_stop_email">'+this.routeRecord[x].email+'</p>';
                      html += '</li>';
                      html += '<li  style="list-style-type: none; padding-top: 5px; display: block; width: 100%;" >';
                          html += '<label>Type d arrêt</label>';
                          html += '<p class="" id="view_stop_type">'+this.routeRecord[x].stopType+'</p>';
                      html += '</li>';
                      html += '<li  style="list-style-type: none; padding-top: 5px; display: block; width: 100%;" >';
                          html += '<label>Priorité</label>';
                          html += '<p class="" id="view_stop_priority">'+this.routeRecord[x].priority+'</p>';
                      html += '</li>';
                      html += '<li  style="list-style-type: none; padding-top: 5px; display: block; width: 100%;" >';
                          html += '<label>Notes</label>';
                          html += '<p class="" id="view_stop_notes" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="">'+this.routeRecord[x].notes+'</p>';
                      html += '</li>';
                      html += '<li  style="list-style-type: none; padding-top: 5px; display: block; width: 100%;" >';
                          html += '<label>Time window</label>';
                          html += '<p class="" id="view_stop_time_window">'+this.routeRecord[x].serviceTime+'</p>';
                      html += '</li>';
                    html += '</ul>';
                    html += '</div>';
                    html += '</div>';

                    this.newObject = {
                      "id":this.routeRecord[x]._id,
                      "timestamp": this.routeRecord[x].address,
                      "htmlView":html
                    };
                    this.markers.push(this.newObject);
                
                
                 
              }
              
              
              this.calculateAndDisplayRoute(directionsService, directionsRenderer);
            }
          });
      });
    });

    

      /*
      */
  }  
  
  mapInit(){
    
    //this.map = new google.maps.Map(this.mapElement.nativeElement,
    //this.mapOptions);

    this.map = new google.maps.Map(this.mapElement.nativeElement, {
      zoom: 6,
      center: { lat: 48.8592877, lng: 2.3310465 },
    });
    this.directionsRendererccc = new google.maps.DirectionsRenderer();
    this.directionsRendererccc.setMap(this.map);
  }

  calculateAndDisplayRoute(directionsService:any, directionsRenderer:any) {
    //console.log("driverRecord "+this.driverRecord.startAddress);
    //console.log("startEndRoute "+JSON.stringify(this.startEndRoute));
    
    //console.log("this.markers "+ JSON.stringify(this.markers[0]));
    const waypts = [];
    const checkboxArray = this.markers;
    //console.log("hereee 120")
    for (let i = 0; i < checkboxArray.length; i++)
    {
      waypts.push({
        location: checkboxArray[i].timestamp,
        stopover: true,
      });
    }
    //console.log("hereee waypts "+ JSON.stringify(waypts));
    
    directionsService
    .route({
      origin: this.startEndRoute[0].address,
      destination: this.startEndRoute[1].address,
      waypoints: waypts,
      optimizeWaypoints: true,
      travelMode: google.maps.TravelMode.DRIVING,
    }).then((response:any) => {

      //console.log("here 208")
      //console.log("response "+ JSON.stringify(response));
      //console.log(directionsRenderer);
      this.directionsRendererccc.setDirections(response);
      const route = response.routes[0];
      //console.log("route "+JSON.stringify(route));
      
      console.log(route.waypoint_order);
      //console.log("response.routes[ 0 ].legs.length "+response.routes[ 0 ].legs.length);
      var lastValOfLoop = response.routes[ 0 ].legs.length -1;
      for(let k=0; k<response.routes[ 0 ].legs.length; k++)
      {
        //console.log("k "+k);
        console.log("route.legs[i].distance!.text "+JSON.stringify(route.legs[k].distance));
        //console.log("route.legs[i].distance!.text "+route.legs[k].distance!.text);
        console.log("route.legs[i].time "+JSON.stringify(route.legs[k].duration));
        
        let distance_value = route.legs[k].distance!.value;
        distance_value = distance_value / 1000;
        let abc = Math.round(distance_value * 10) / 10;
        console.log("abc "+abc);
        this.totalDistance = this.totalDistance + abc;

        let duration_value = Number(route.legs[k].duration.value);
        duration_value = duration_value / 60;
        let xyz = duration_value.toFixed(0);
        this.totalTimeInMinute = this.totalTimeInMinute + parseFloat(xyz);
        console.log("xyz "+xyz);
        
        let m = k+1;
        let leg = response.routes[ 0 ].legs[ k ];
        //console.log(route.waypoint_order[k]);
        console.log("leg.end_address "+leg.end_address);
        if(k > 0)
        {
          console.log("iffffffffffffffffff start");
          let distance_value = route.legs[k-1].distance!.value;
          distance_value = distance_value / 1000;
          let abc = Math.round(distance_value * 10) / 10;
          console.log("abc "+abc);

          let duration_value = Number(route.legs[k-1].duration.value);
          duration_value = duration_value / 60;
          let xyz = duration_value.toFixed(0);
          console.log("xyz "+xyz);


          let o = route.waypoint_order[k-1];
          //let p = o+2;
          let p = o;
          //console.log("oooooo "+o);
          this.html_view = this.markers[p].htmlView;
          //console.log("this.html_view iffff"+this.html_view);
          console.log("kkkk ------>>> "+k);
          console.log("ppppp ------>>> "+p);
          console.log("this.html_view iffff "+this.markers[p].id);
          this.getqueryParam = {"position":k,"id":this.markers[p].id,distance:abc,distanceTime:xyz};
          this._http.post(this.updatePositionRoute,this.getqueryParam).subscribe((response:any)=>{

          });


        }else{
          //this.html_view = this.markers[k].htmlView;
          this.html_view = "";
          //console.log("this.html_view elseeee "+this.markers[k].id);
          //console.log("this.html_view elseeee "+this.markers[k]);
        }

        this.makeMarker( leg.start_location, 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld='+m+'|000000|FFFFFF',response.routes[ 0 ].legs[ k ].start_address,this.html_view );

        if(m == response.routes[ 0 ].legs.length)
        {
          //console.log("iffff");
          let p = m+1;
          let mn = { lat: 48.8592877, lng: 2.3310465 };
          //this.makeMarker( leg.end_location, 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld='+p+'|000000|FFFFFF',response.routes[ 0 ].legs[ k ].end_address,this.markers[1].htmlView );
          this.makeMarker( leg.end_location, 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld='+p+'|000000|FFFFFF',response.routes[ 0 ].legs[ k ].end_address,"" );
        }
        if(lastValOfLoop == k)
        {
          
          console.log("last val of k "+k);
          console.log("this.route_id "+this.route_id);
          //console.log("id ----->>>> "+this.markers[k].id);
          let distance_value = route.legs[k].distance!.value;
          distance_value = distance_value / 1000;
          let abc = Math.round(distance_value * 10) / 10;
          console.log("abc "+abc);

          let duration_value = Number(route.legs[k].duration.value);
          duration_value = duration_value / 60;
          let xyz = duration_value.toFixed(0);
          console.log("xyz "+xyz);
          this.getqueryParam = {"position":k+1,"route_id":this.route_id,distance:abc,distanceTime:xyz};
          this._http.post(this.updatePositionRouteLast,this.getqueryParam).subscribe((response:any)=>{

          });

        }
        //console.log(this.markers[k]);
        //console.log("checkboxArray --->   "+JSON.stringify(checkboxArray[k]));
        // console.log("id "+this.markers[k].id);
        // console.log("id "+this.markers[k].timestamp);
        // console.log(response.routes[ 0 ].legs[ k ]);
        // console.log(response.routes[ 0 ].legs[ k ].start_address);
        //console.log("leg.start_address "+response.routes[ 0 ].legs[ k ].start_address);
        //console.log("leg.end_location "+leg.end_location);
      }
      console.log(" this.totalDistance "+ this.totalDistance);
      console.log(" this.totalTimeInMinute "+ this.totalTimeInMinute);
      
      this.totalHour = Math.floor(this.totalTimeInMinute / 60);
      this.totalMin = this.totalTimeInMinute - (this.totalHour * 60);

      console.log("this.totalHour "+this.totalHour);
      console.log("this.totalMin "+this.totalMin)
      this.getqueryParam = 
      {
        "totalHour":this.totalHour,
        "totalMin":this.totalMin,
        "route_id":this.route_id,
        "totalDistance":this.totalDistance
      };
      this._http.post(this.updateTotalTimeDistance,this.getqueryParam).subscribe((response:any)=>{

      });
      //console.log(response.routes[ 0 ].legs[ 0 ]);
      //console.log("sdfdf "+ JSON.stringify(response.routes[ 0 ].legs[ 0 ]));
    }).catch((e:any) => window.alert("Directions request failed due to " + e));
  }
  makeMarker( position:any, icon:any, title:any,htmlss:any )
  {
    const infowindow = new google.maps.InfoWindow({
      content: htmlss,
      ariaLabel: "Uluru",
    });
    const marker = new google.maps.Marker({
     position: position,
     map: this.map,
     icon:icon,
     title: title
    });
    marker.addListener("click", () => {
      infowindow.open({
        anchor: marker,
        map:this.map,
      });
    });
    
  }
  finalRouteAssign()
  {
    console.log("here click fun");
    this.getqueryParam = {"route_id":this.route_id,"driver_id":this.driver_id};
    this._http.post(this.finalRouteAssignApi,this.getqueryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        //this.form.reset();
        //this.myFiles = [];
        setTimeout(() => {
          //window.location.reload();
          console.log(this.base_url+"allRoute");
          window.location.href = this.base_url+"allRoute";
        }, 2000); 
      }
    });
  }
  assignToDriver(route_id:any,driver_id:any)
  {
    console.log("route_id "+route_id);
    console.log("driver_id "+driver_id);
    this.getqueryParam = {"route_id":route_id,"driver_id":driver_id};
    this._http.post(this.assignToDriverApi,this.getqueryParam).subscribe((response:any)=>{
      console.log(response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        
        //this.form.reset();
        //this.myFiles = [];
        setTimeout(() => {
          //window.location.reload();
          window.location.href = this.base_url+"allRoute";
        }, 2000); 
      }

    });
  }
  get f(){
    return this.productForm.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  onSubmit()
  {
    if(this.productForm.valid)
    {
      let  today = new Date(this.productForm.value.start_date );
      //const today = new Date();
      const yyyy = today.getFullYear();
      let mm = today.getMonth() + 1; // Months start at 0!
      let dd = today.getDate();
      
      let  todayy = new Date(this.productForm.value.start_time );
      let hourr = todayy.getHours();
      let minuee = todayy.getMinutes();

      const formattedToday = dd + '/' + mm + '/' + yyyy;

      console.log("myDateee ",hourr,minuee);
      console.log("myDateee ",formattedToday);
      let start_date_time = formattedToday+" "+hourr+"h"+minuee;
      let  start_time = hourr+"h"+minuee;
      this.getqueryParam = {"route_id":this.productForm.value.route_id,"driver_id":this.productForm.value.driver_id,start_date:formattedToday,start_time:start_time,start_date_time:start_date_time};
      this._http.post(this.assignToDriverApi,this.getqueryParam).subscribe((response:any)=>{
        console.log(response);
        this.apiResponse = response;
        if(this.apiResponse.error == false)
        {
          
          //this.form.reset();
          //this.myFiles = [];
          setTimeout(() => {
            //window.location.reload();
            window.location.href = this.base_url+"allRoute";
          }, 2000); 
        }

    });
    }else{
      console.log('erro form submitted');
      this.validateAllFormFields(this.productForm); 
      // validate all form fields
    }
  }

}

