import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllUserCancledOrderComponent } from './all-user-cancled-order.component';

describe('AllUserCancledOrderComponent', () => {
  let component: AllUserCancledOrderComponent;
  let fixture: ComponentFixture<AllUserCancledOrderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllUserCancledOrderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllUserCancledOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
