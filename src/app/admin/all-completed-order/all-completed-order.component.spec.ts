import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllCompletedOrderComponent } from './all-completed-order.component';

describe('AllCompletedOrderComponent', () => {
  let component: AllCompletedOrderComponent;
  let fixture: ComponentFixture<AllCompletedOrderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllCompletedOrderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllCompletedOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
