import { Component, OnInit } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { EditprofileComponent } from '../editprofile/editprofile.component';
import { HttpClient } from '@angular/common/http';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
@Component({
  selector: 'app-refunds',
  templateUrl: './refunds.component.html',
  styleUrls: ['./refunds.component.css']
})
export class RefundsComponent implements OnInit {
  searchText:any; select_all = false; apiResponse:any; users:any; orders:any;
  base_url:any; invoice_id:any[] = [];
  page_no:number=0;numbers:any;selectedIndex: number=0;
  constructor(public dialog: MatDialog,private http: HttpClient,private loginAuthObj:LoginauthenticationService) { 
    this.users =[]
    this.orders = []
    this.base_url = loginAuthObj.baseapiurl;

    let queryParam1 = {pageno:null};
    this.http.post(this.base_url+"getallrefunds",queryParam1).subscribe(res => {
      console.log(res);
      this.apiResponse = res
      //this.orders = this.apiResponse.data
      this.page_no = this.apiResponse.pages;
        this.numbers = Array(this.page_no).fill(0).map((m,n)=>n);
    });
    this.getallColor(0)
  }
  getallColor(pageno:number=0)
  {
    let queryParam = {pageno:pageno};
    this.http.post(this.base_url+"getallrefunds",queryParam).subscribe(res => {
      console.log(res);
      this.apiResponse = res
      this.orders = this.apiResponse.data
      
    })
  }
  ngOnInit(): void {
    
  }
  markAsdoneFun(id:any)
  {
    this.http.get(this.base_url+"markAsdone/"+id).subscribe(res => {
      this.apiResponse = res;
      //alert("user deleted successfully")
      // console.log(this.apiResponse)
      // console.log("status "+this.apiResponse.status)
      if(this.apiResponse.status == true)
      {
        setTimeout(() => {
          window.location.reload();
        }, 1000); 
      }
    });
  }
  markAsundoneFun(id:any)
  {
    this.http.get(this.base_url+"markAsundone/"+id).subscribe(res => {
      this.apiResponse = res;
      //alert("user deleted successfully")
       console.log(this.apiResponse)
      // console.log("status "+this.apiResponse.status)
      if(this.apiResponse.status == true)
      {
        setTimeout(() => {
          window.location.reload();
        }, 1000); 
      }
    });
  }

  onSelectAll(e: any): void { 
    //console.log("is checked "+e);
    for (let i = 0; i < this.orders.length; i++)
     {
       const item = this.orders[i];
       item.is_checked = e;
       
     }
 
    if(e == true)
    {
     for (let i = 0; i < this.orders.length; i++)
     {
       const item = this.orders[i];
       item.is_checked = e;
       //console.log("_id" +item._id);
       let cc = item._id;
       this.invoice_id.push(cc);
     }
    }else{
     this.invoice_id = [];
    }
    //console.log("invoiceeee "+this.invoice_id);
   }
   onSelectAll2(id:any,event:any)
   {
     console.log("id"+id);
     console.log("event"+event);
     if(event == true)
     {
       this.invoice_id.push(id);
     }else{
       const index = this.invoice_id.indexOf(id);
       this.invoice_id.splice(index, 1)
     }
 
     
   }

   form = new UntypedFormGroup({
    action_delete: new UntypedFormControl('', [])
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
   submit()
   {
      console.log(this.form.value);
      if(this.form.value.action_delete == "approve")
      {
        //console.log("approve");
        //console.log("invoiceeee single select"+this.invoice_id);
        if(this.invoice_id.length > 0)
        {
          var x=0;
          var less_val = this.invoice_id.length - 1;
          for(let i=0; i<this.invoice_id.length; i++)
          {
            console.log("invoiceeee single select"+this.invoice_id[i]);
            this.http.get(this.base_url+"markAsdone/"+this.invoice_id[i]).subscribe(res => {
              this.apiResponse = res;
              if(less_val == x)
              {
                if(this.apiResponse.status == true)
                {
                  setTimeout(() => {
                    window.location.reload();
                  }, 2000);
                }
              }

              x++;
            });
          }
        }
      }else if(this.form.value.action_delete == "cancel")
      {
        console.log("cancel");
        //console.log("invoiceeee single select"+this.invoice_id);
        if(this.invoice_id.length > 0)
        {
          var x=0;
          var less_val = this.invoice_id.length - 1;
          for(let i=0; i<this.invoice_id.length; i++)
          {
            //console.log("invoiceeee single select"+this.invoice_id[i]);
            this.http.get(this.base_url+"markAsundone/"+this.invoice_id[i]).subscribe(res => {
              this.apiResponse = res;
              if(less_val == x)
              {
                if(this.apiResponse.status == true)
                {
                  setTimeout(() => {
                    window.location.reload();
                  }, 2000);
                }
              }

              x++;
            });
          }
        }
      }
    }

}
