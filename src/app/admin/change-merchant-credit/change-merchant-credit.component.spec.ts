import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeMerchantCreditComponent } from './change-merchant-credit.component';

describe('ChangeMerchantCreditComponent', () => {
  let component: ChangeMerchantCreditComponent;
  let fixture: ComponentFixture<ChangeMerchantCreditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChangeMerchantCreditComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ChangeMerchantCreditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
