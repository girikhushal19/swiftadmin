import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-change-merchant-credit',
  templateUrl: './change-merchant-credit.component.html',
  styleUrls: ['./change-merchant-credit.component.css']
})
export class ChangeMerchantCreditComponent implements OnInit {
  imageSrc: string = '';
  fileInputLabel: string = "";
  myFiles:string [] = [];
  base_url = "";base_url_node = ""; 
  token:any;formData:any;images:any;video:any;user_type:any;apiResponse:any;formValue:any;editDriverSubmit:any;getSingleDriverApi:any;allModelList:any;getCarCategory:any;allCarCategory:any;
  edit_id:any;getqueryParam:any;record:any;
  default_date: Date = new Date(); default_date2: Date = new Date();
  default_date3: Date = new Date(); default_date4: Date = new Date();
  //ddDate = new Date('Thu Jan 26 2023 22:38:32 GMT+0530 (India Standard Time)');
  startAddress: string = '';startLatitude: string = '';startLongitude: string = '';
  endAddress: string = '';endLatitude: string = '';endLongitude: string = '';
  allFormValue:any; old_credit_limit:number=0;
  old_vehicleType:any;old_fullName:any;old_email:any;old_mobileNumber:any;old_id:any;
  old_shiftStartTime:any;old_shiftEndTime:any;old_breakStartTime:any;old_breakEndTime:any;
  old_startAddress:any;old_endAddress:any;old_startLocation:any;old_endLocation:any;cordinate:any; 
  base_url_node_only:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    //http://localhost:9000/admin-panel/getSingleUsers/63e60d67425b24e4e59f58e3
    this.editDriverSubmit = this.base_url_node+"updateCreditLimit";
    
    //this.getCarCategory = this.base_url_node+"getCarCategory";
    //console.log("here");
    this.edit_id = this.actRoute.snapshot.params['id'];
    this.getSingleDriverApi = this.base_url_node_only+"admin-panel/getSingleUsers/"+this.edit_id;
    this.getqueryParam = {id:this.edit_id};
    this._http.get(this.getSingleDriverApi ).subscribe((response:any)=>{
      //console.log("getSingleUser"+response);
      //this.apiResponse = response;
      if(response.status == true)
      {
        this.record = response;
      
        //console.log("record "+JSON.stringify(this.record)); 
        //console.log(this.record);
        if(this.record.data.length > 0)
        {
          this.old_vehicleType = this.record.data[0];
          console.log("this.old_vehicleType ",this.old_vehicleType);
          if(this.old_vehicleType.credit_limit)
          {
            this.old_credit_limit = this.old_vehicleType.credit_limit;
          }
          
        }
       

      }  
       
  
        /*console.log(this.record._id); 
        console.log(this.record.price); */
        //console.log(this.old_category); 
      });
  }
  ngOnInit(): void {
  }



  form = new UntypedFormGroup({
    credit_limit: new UntypedFormControl('', [Validators.required]),
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
 
  submit()
  {
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      console.log(this.form.value);
      this.allFormValue = {
        "credit_limit":this.form.value.credit_limit,
        "id":this.edit_id,
      };
      
      this._http.post(this.editDriverSubmit,this.allFormValue).subscribe((response:any)=>{
        //console.log("response of api"+response);
        this.apiResponse = response;
        if(this.apiResponse.error == false)
        {
          //this.form.reset();
          //this.myFiles = [];
          setTimeout(() => {
            window.history.back();
          }, 2000);
        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}
