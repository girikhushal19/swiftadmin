import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllDeletedUsersComponent } from './all-deleted-users.component';

describe('AllDeletedUsersComponent', () => {
  let component: AllDeletedUsersComponent;
  let fixture: ComponentFixture<AllDeletedUsersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllDeletedUsersComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllDeletedUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
