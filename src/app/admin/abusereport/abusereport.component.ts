import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
@Component({
  selector: 'app-abusereport',
  templateUrl: './abusereport.component.html',
  styleUrls: ['./abusereport.component.css']
})
export class AbusereportComponent implements OnInit {
  searchText:any;
  page_no:number=0;numbers:any;selectedIndex: number=0;
  base_url:any;vendors:any;products:any;reasons:any;
  abuseReportArray:any;base_url_two:string="";apiResponse:any;
  abuseReport:any;deletemultipleseller:string="";
  invoice_id:any[] = [];
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService,private http: HttpClient) {
    this.base_url = loginAuthObj.baseapiurl
    this.abuseReportArray =[]
    this.abuseReport = []
    this.base_url_two = loginAuthObj.baseapiurl2;
    //this.deletemultipleseller = this.base_url_two+"api/seller/deletemultipleseller";
    this.base_url_two = loginAuthObj.baseapiurl2
    this.vendors = []
    
    this.http.get(this.base_url_two+"api/seller/getallsellerforfilter",{}).subscribe(res => {
      //console.log(res);
      this.apiResponse = res;
      this.vendors =this.apiResponse.data;
      //console.log(this.vendors);
    });

    this.http.get(this.base_url_two+"api/product/getallproductjustforfilter",{}).subscribe(res => {
      //console.log(res);
      this.apiResponse = res;
      this.products =this.apiResponse.data;
      //console.log("products -> "+this.products);
    })

    this.http.get(this.base_url_two+"api/admin/getallreasons",{}).subscribe(res => {
      //console.log(res);
      this.apiResponse = res;
      this.reasons =this.apiResponse.data;
      console.log("reasons -> "+this.reasons);
    })

    let queryParam1 = {pageno:null};
    this.http.post(this.base_url+"getallreports",queryParam1).subscribe(res => {
      this.abuseReportArray = res
      //this.abuseReport =this.abuseReportArray.data
      this.page_no = this.abuseReportArray.pages;
        this.numbers = Array(this.page_no).fill(0).map((m,n)=>n);
      //console.log("this.abuseReport ---->>>>> "+ JSON.stringify(this.abuseReport));
    })
    this.getallColor(0)
   }
   getallColor(pageno:number=0)
   {
    this.selectedIndex = pageno;
    let queryParam = {pageno:pageno};
      this.http.post(this.base_url+"getallreports",queryParam).subscribe(res => {
        this.abuseReportArray = res;

        this.abuseReport =this.abuseReportArray.data
        console.log("this.abuseReport "+JSON.stringify(this.abuseReport));
        //console.log("this.abuseReport ---->>>>> "+ JSON.stringify(this.abuseReport));
      })
   }
  ngOnInit(): void {
    
  }
  select_all = false;
  data: any[] = [
  ]

  onSelectAll(e: any): void { 
    //console.log("is checked "+e);
    for (let i = 0; i < this.abuseReport.length; i++)
     {
       const item = this.abuseReport[i];
       item.is_checked = e;
       
     }
 
    if(e == true)
    {
     for (let i = 0; i < this.abuseReport.length; i++)
     {
       const item = this.abuseReport[i];
       item.is_checked = e;
       //console.log(item._id);
       let cc = item._id;
       this.invoice_id.push(cc);
     }
    }else{
     this.invoice_id = [];
    }
    console.log("invoiceeee "+this.invoice_id);
   }
   onSelectAll2(id:any,event:any)
   {
     console.log("id"+id);
     console.log("event"+event);
     if(event == true)
     {
       this.invoice_id.push(id);
     }else{
       const index = this.invoice_id.indexOf(id);
       this.invoice_id.splice(index, 1)
     }
 
     console.log("invoiceeee single select"+this.invoice_id);
   }


   form = new UntypedFormGroup({
    action_delete: new UntypedFormControl('', [])
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  submit()
   {
      console.log(this.form.value);
      if(this.form.value.action_delete == "approve")
      {
        //console.log("approve");
        //console.log("invoiceeee single select"+this.invoice_id);
        if(this.invoice_id.length > 0)
        {
          var x=0;
          var less_val = this.invoice_id.length - 1;
          for(let i=0; i<this.invoice_id.length; i++)
          {
            console.log("invoiceeee single select"+this.invoice_id[i]);
            this.http.get(this.base_url+"markAsresolved/"+this.invoice_id[i]).subscribe(res => {
              this.apiResponse = res;
              if(less_val == x)
              {
                if(this.apiResponse.status == true)
                {
                  setTimeout(() => {
                    window.location.reload();
                  }, 2000);
                }
              }

              x++;
            });
          }
        }
      }else if(this.form.value.action_delete == "cancel")
      {
        console.log("cancel");
        //console.log("invoiceeee single select"+this.invoice_id);
        if(this.invoice_id.length > 0)
        {
          var x=0;
          var less_val = this.invoice_id.length - 1;
          for(let i=0; i<this.invoice_id.length; i++)
          {
            //console.log("invoiceeee single select"+this.invoice_id[i]);
            this.http.get(this.base_url+"markAsUnresolved/"+this.invoice_id[i]).subscribe(res => {
              this.apiResponse = res;
              if(less_val == x)
              {
                if(this.apiResponse.status == true)
                {
                  setTimeout(() => {
                    window.location.reload();
                  }, 2000);
                }
              }

              x++;
            });
          }
        }
      }
    }
  markResolve(id:any)
  {
    console.log(id);
    //{{local}}/admin-panel//63bd435dee7014ce11fe2cf3
    this.http.get(this.base_url+"markAsresolved/"+id).subscribe(res => {
      this.apiResponse = res;
      //alert("user deleted successfully")
      console.log(this.apiResponse)
      console.log("status "+this.apiResponse.status)
      if(this.apiResponse.status == true)
      {
        setTimeout(() => {
          window.location.reload();
        }, 1000); 
      }
      //console.log(res)
    });
  }

  markUnResolve(id:any)
  {
    console.log(id);
    //{{local}}/admin-panel//63bd435dee7014ce11fe2cf3
    this.http.get(this.base_url+"markAsUnresolved/"+id).subscribe(res => {
      this.apiResponse = res;
      //alert("user deleted successfully")
      console.log(this.apiResponse)
      console.log("status "+this.apiResponse.status)
      if(this.apiResponse.status == true)
      {
        setTimeout(() => {
          window.location.reload();
        }, 1000); 
      }
      //console.log(res)
    });
  }




  formSecondStep = new UntypedFormGroup({
    reason_f: new UntypedFormControl('', []),
    product_f: new UntypedFormControl('', []),
    vendor_f: new UntypedFormControl('', []),
  });

  get formSecondStepFun(){
    return this.formSecondStep.controls;
  }

  submit2()
  {
    console.log(this.formSecondStep.value);
    let queryParam = { "product_id":this.formSecondStep.value.product_f,"vendor_id":this.formSecondStep.value.vendor_f,"reason":this.formSecondStep.value.reason_f};
    this.http.post(this.base_url+"getallreports",queryParam).subscribe(res => {
      this.abuseReportArray = res
      this.abuseReport =this.abuseReportArray.data
      //console.log("this.abuseReport ---->>>>> "+ JSON.stringify(this.abuseReport));
    })
  }

}
