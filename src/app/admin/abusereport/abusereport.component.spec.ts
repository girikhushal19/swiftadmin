import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AbusereportComponent } from './abusereport.component';

describe('AbusereportComponent', () => {
  let component: AbusereportComponent;
  let fixture: ComponentFixture<AbusereportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AbusereportComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AbusereportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
