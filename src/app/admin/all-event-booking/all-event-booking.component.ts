import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';


@Component({
  selector: 'app-all-event-booking',
  templateUrl: './all-event-booking.component.html',
  styleUrls: ['./all-event-booking.component.css']
})
export class AllEventBookingComponent implements OnInit {
  searchText:any;
  base_url:any; 
  vendorsarray:any;base_url_two:string="";apiResponse:any;
  vendors:any;deletemultipleseller:string="";
  vendorsCount:number = 0;numbers:any; selectedIndex: number=0;
  invoice_id:any[] = [];id:string="";
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService,private http: HttpClient, private activeRoute: ActivatedRoute) {
    this.base_url = loginAuthObj.baseapiurl
    this.vendorsarray =[]
    this.vendors = []
    this.base_url_two = loginAuthObj.baseapiurl2;
    //this.deletemultipleseller = this.base_url_two+"api/seller/deletemultipleseller";

    this.id = this.activeRoute.snapshot.params['id'];
    console.log(" this.id"+ this.id);


   }

  ngOnInit(): void {
    this.http.get(this.base_url_two+"api/event/getallbookingbyeventidCount/"+this.id).subscribe(res => {
      this.apiResponse = res;
      //console.log(this.apiResponse);
       this.vendorsCount = this.apiResponse.data;
       this.numbers = Array(this.vendorsCount).fill(0).map((m,n)=>n);
       console.log("this.numbers "+this.numbers);
    })
    
    this.getallBanner(0)
  }
  getallBanner(numofpage=0){
    this.selectedIndex = numofpage;
    this.http.post(this.base_url_two+"api/event/getallbookingbyeventid",{"pageno":numofpage,"id":this.id}).subscribe(res => {
      this.apiResponse = res;
      //console.log(this.apiResponse);
       this.vendors = this.apiResponse.data;
      
    })
  }
  select_all = false;
  data: any[] = []
}
