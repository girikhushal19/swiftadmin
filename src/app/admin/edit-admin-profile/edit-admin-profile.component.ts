import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-edit-admin-profile',
  templateUrl: './edit-admin-profile.component.html',
  styleUrls: ['./edit-admin-profile.component.css']
})
export class EditAdminProfileComponent implements OnInit {
   
  imageSrc: string = '';
  fileInputLabel: string = "";
  myFiles:string [] = [];
  base_url = "";base_url_node = ""; base_url_node_only = ""; 
  token:any;formData:any;images:any;video:any;user_type:any;apiResponse:any;formValue:any;editAdminProfileSubmit:any;getModel:any;allModelList:any;getCarCategory:any;allCarCategory:any;getAdminProfile:any;getqueryParam:any;removeQueryParam:any;old_category:any;old_price:any;old_images:any;record:any;removeCategoryImage:any;old_email:any;old_firstName:any;old_lastName:any;old_userImage:any;old_id:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  {

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    //console.log("this.base_url"+this.base_url);
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.editAdminProfileSubmit = this.base_url_node+"editAdminProfileSubmitApi";
    //this.getCarCategory = this.base_url_node+"getCarCategory";
    //console.log("here");




    ///this.edit_id = this.actRoute.snapshot.params['id'];
    //console.log(this.edit_id);
    let loggedInAdminId = this.loginAuthObj.userLoggedIn();
    this.getqueryParam = {"id":loggedInAdminId};
    this.getAdminProfile = this.base_url_node+"getAdminProfile"; 
    this._http.post(this.getAdminProfile,this.getqueryParam).subscribe((response:any)=>{
    console.log("getAdminProfile ",response);
    //this.apiResponse = response;
      this.record = response.record;

      //console.log(this.record); 
      //console.log(this.record[0]);
      this.old_email = this.record.email;
      this.old_firstName = this.record.firstName;
      this.old_lastName = this.record.lastName;  
      this.old_id = this.record._id; 
      this.old_userImage = this.record.adminImage; 

      /*this.old_category = this.record[0].category;
      this.old_price = this.record[0].price;
      this.old_images = this.record[0].images;*/
      /*console.log(this.record[0]._id); 
      console.log(this.record[0].price); */
      //console.log(this.old_category); 
    });

   }
   get f(){
    return this.form.controls;
  }
  ngOnInit(): void {
      this.myFiles = [];
  }

  form = new UntypedFormGroup({
    email: new UntypedFormControl('', [Validators.required]),
    firstName: new UntypedFormControl('', [Validators.required]),
    lastName: new UntypedFormControl('', [Validators.required]),
    images: new UntypedFormControl('', []),
  });
  
  onFileChange(event:any)
  {
    this.myFiles = [];
      for (var i = 0; i < event.target.files.length; i++)
      { 
          this.myFiles.push(event.target.files[i]);
      }
  } 

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
   

   

  submit()
  {
    
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      
       this.formData = new FormData(); 
        this.formData.append('firstName', this.form.value.firstName);
        this.formData.append('lastName', this.form.value.lastName);
        this.formData.append('email', this.form.value.email);
        this.formData.append('id', this.old_id);

      //this.formData.append('file', this.images);

        for (var i = 0; i < this.myFiles.length; i++)
        { 
          this.formData.append("file", this.myFiles[i]);
        } 
        
      //this.formData.append('parent_id', this.form.value.parent_id);
      //this.formData.append('attribute_type', this.form.value.attribute_type);
      //this.formData.append('images[]', this.myFiles[0]);
      
      //console.log("form valuess");
      //console.log(this.formData);
      
      /*this.formData.append('model_name', this.form.value.model_name);
      this.formData.append('parent_id', this.form.value.parent_id);
      this.formData.append('attribute_type', this.form.value.attribute_type);*/
      //formData.append('file', this.images);
      /*this.formValue = this.form.value;
      this.formValue = this.images;*/
      //console.log(this.form.value);
      ///console.log(this.form.value.model_name);
      

      this._http.post(this.editAdminProfileSubmit,this.formData).subscribe((response:any)=>{
          console.log("response of api"+response);
          this.apiResponse = response;
         if(this.apiResponse.error == false)
          {
            setTimeout(() => {
                window.location.reload();
            }, 2000); 
          }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }


}