import { Component, OnInit,HostListener, ViewChild, ElementRef,AfterViewInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-edit-stop',
  templateUrl: './edit-stop.component.html',
  styleUrls: ['./edit-stop.component.css']
})
export class EditStopComponent implements OnInit {
  getqueryParam:any;
  imageSrc: string = '';
  fileInputLabel: string = "";
  myFiles:string [] = [];
  base_url = "";base_url_node = ""; queryParam:any;allStops:any;
  token:any;formData:any;images:any;video:any;user_type:any;apiResponse:any;formValue:any;editRouteSubmit:any;getModel:any;allModelList:any;getAllRouteStop:any;allCarCategory:any;
  editRouteFileSubmit:any;route_id:any;
  default_date: Date = new Date(); default_date2: Date = new Date();
  default_date3: Date = new Date(); default_date4: Date = new Date();
  //ddDate = new Date('Thu Jan 26 2023 22:38:32 GMT+0530 (India Standard Time)');
  address: string = '';latitude: string = '';longitude: string = '';
  endAddress: string = '';endLatitude: string = '';endLongitude: string = '';
  allFormValue:any;getStopList:any;
  numericArray: number [] = [];
  Normal:string = "Normal";
  startPoint: string = '';endPoint: string = '';
  displayStyle = "none";
  displayStyle2 = "none";
  
  @HostListener('paste', ['$event']) blockPaste(e: KeyboardEvent) {
    e.preventDefault();
  }

  @HostListener('copy', ['$event']) blockCopy(e: KeyboardEvent) {
    e.preventDefault();
  }

  @HostListener('cut', ['$event']) blockCut(e: KeyboardEvent) {
    e.preventDefault();
  }
  record:any;getSingleStopApi:any;
  old_notes:any;old_fullName:any;old_email:any;old_mobileNumber:any;old_id:any;
  old_priority:any;old_serviceTime:any;old_businessName:any;old_parcelCount:any;
  old_startAddress:any;old_endAddress:any;old_startLocation:any;old_endLocation:any;cordinate:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.editRouteSubmit = this.base_url_node+"editRouteSubmit";
    this.editRouteFileSubmit = this.base_url_node+"editRouteFileSubmit";
    //this.getModel = this.base_url_node+"getModel";
    this.getAllRouteStop = this.base_url_node+"getAllRouteStop";
    //console.log("here");
    for(let x=1; x<=60; x++)
    {
      this.numericArray.push(x);
    }
    this.route_id = this.actRoute.snapshot.params['id'];
    marker: google.maps.Marker;

    this.getSingleStopApi = this.base_url_node+"getSingleStopApi";
    this.getqueryParam = {"route_id":this.route_id};
    this._http.post(this.getSingleStopApi,this.getqueryParam).subscribe((response:any)=>{
      //console.log("getSingleUser"+response);
      //this.apiResponse = response;
      if(response.error == false)
      {
        this.record = response.record;
      
        console.log("record "+JSON.stringify(this.record)); 
        //console.log(this.record);
        
        this.old_fullName = this.record.fullName;
        this.old_email = this.record.email; 
        this.old_mobileNumber = this.record.mobileNumber; 
        this.old_notes = this.record.notes; 
        this.old_serviceTime = this.record.serviceTime; 
        this.old_businessName = this.record.businessName;
        this.old_id = this.record._id; 
        this.address = this.record.address;
        this.old_priority = this.record.priority;
        this.old_startLocation = this.record.location;
        this.old_parcelCount = this.record.parcelCount;
        if(this.old_startLocation)
        {
          if(this.old_startLocation.coordinates)
          {
            ///console.log("old_endLocation "+this.old_startLocation.coordinates);
            if(this.old_startLocation.coordinates.length > 0)
            {
              this.latitude = this.old_startLocation.coordinates[0];
              this.longitude =  this.old_startLocation.coordinates[1];
              //  console.log("startLatitude"+this.startLatitude);
              //  console.log("startLongitude"+this.startLongitude);
            }
          }
        }

      }  
       
  
        /*console.log(this.record._id); 
        console.log(this.record.price); */
        //console.log(this.old_category); 
      });

  }

  ngOnInit(): void {
    this.myFiles = []; 
  }

  handleAddressChange(address: any)
  {
    //console.log(address);
    this.address = address.formatted_address
    this.latitude = address.geometry.location.lat()
    this.longitude = address.geometry.location.lng()
  }

  form = new UntypedFormGroup({
    fullName: new UntypedFormControl('', []),
    mobileNumber: new UntypedFormControl('', []),
    notes: new UntypedFormControl('', []),
    priority: new UntypedFormControl('', [Validators.required]),
    serviceTime: new UntypedFormControl('', []),
    address: new UntypedFormControl('', [Validators.required]),
    businessName: new UntypedFormControl('', []),
    email: new UntypedFormControl('', []),
    stopType: new UntypedFormControl('', []),
    parcelFront: new UntypedFormControl('', []),
    parcelLeft: new UntypedFormControl('', []),
    parcelFloor: new UntypedFormControl('', []),
    parcelCount: new UntypedFormControl('', []),
    images: new UntypedFormControl('', []),
    route_id: new UntypedFormControl('', []),
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  onFileChange(event:any)
  {
    this.myFiles = [];
      for (var i = 0; i < event.target.files.length; i++)
      { 
          this.myFiles.push(event.target.files[i]);
      }
  }
  submit()
  {
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      console.log(this.myFiles);
      this.formData = new FormData();

      if(this.myFiles.length > 0)
      {
        for (var i = 0; i < this.myFiles.length; i++)
        { 
          console.log(this.myFiles[i]);
          this.formData.append("file", this.myFiles[i]);
        } 
        this.formData.append('fullName', this.form.value.fullName); 
        this.formData.append('notes', this.form.value.notes); 
        this.formData.append('email', this.form.value.email); 
        this.formData.append('mobileNumber', this.form.value.mobileNumber); 
        this.formData.append('priority', this.form.value.priority);
        this.formData.append('serviceTime', this.form.value.serviceTime);
        this.formData.append('address', this.address);
        this.formData.append('businessName', this.form.value.businessName);
        this.formData.append('stopType', this.form.value.stopType);
        this.formData.append('parcelFront', this.form.value.parcelFront); 
        this.formData.append('parcelLeft', this.form.value.parcelLeft); 
        this.formData.append('parcelFloor', this.form.value.parcelFloor); 
        this.formData.append('parcelCount', this.form.value.parcelCount); 
        this.formData.append('latitude', this.latitude); 
        this.formData.append('longitude', this.longitude); 
        this.formData.append('route_id', this.form.value.route_id); 
        
        console.log(this.formData);
        this._http.post(this.editRouteFileSubmit,this.formData).subscribe((response:any)=>{
          //console.log("response of api"+response);
          this.apiResponse = response;
          if(this.apiResponse.error == false)
          {
            
            //this.form.reset();
            //this.myFiles = [];
            setTimeout(() => {
              //window.location.reload();
              window.location.href = this.base_url+"addRoute/"+this.apiResponse.lastInsertId;
            }, 2000); 
          }
        });
        
      }else{
        this.allFormValue = {
          "fullName":this.form.value.fullName,
          "email":this.form.value.email,
          "mobileNumber":this.form.value.mobileNumber,
          "notes":this.form.value.notes,
          "priority":this.form.value.priority,
          "serviceTime":this.form.value.serviceTime,
          "businessName":this.form.value.businessName,
          "stopType":this.form.value.stopType,
          "parcelFront":this.form.value.parcelFront,
          "parcelLeft":this.form.value.parcelLeft,
          "parcelFloor":this.form.value.parcelFloor,
          "parcelCount":this.form.value.parcelCount,
          "route_id":this.form.value.route_id,
          "address":this.address,
          "latitude":this.latitude,
          "longitude":this.longitude,
        };
        this._http.post(this.editRouteSubmit,this.allFormValue).subscribe((response:any)=>{
          //console.log("response of api"+response);
          this.apiResponse = response;
          if(this.apiResponse.error == false)
          {
            //this.form.reset();
            //this.myFiles = [];
            setTimeout(() => {
              //window.location.reload();
              window.location.href = this.base_url+"addRoute/"+this.apiResponse.lastInsertId;
            }, 2000); 
          }
        });
      }
      
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }

}
