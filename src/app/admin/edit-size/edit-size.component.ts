import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';


@Component({
  selector: 'app-edit-size',
  templateUrl: './edit-size.component.html',
  styleUrls: ['./edit-size.component.css']
})
export class EditSizeComponent implements OnInit {



  base_url = "";base_url_node = "";
  token:any;user_type:any;apiResponse:any;formValue:any;editColorsSubmit:any;getCarAttribute:any;getCarCategory:any;allCarAttribute:any;allCarCategory:any;getSingleColor:any;getqueryParam:any;edit_id:any;record:any;old_color_name:any;old_color_code:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.editColorsSubmit = this.base_url_node+"editSizeSubmit";
    this.getSingleColor = this.base_url_node+"getSingleSize";
     
    //console.log("here");
    this.edit_id = this.actRoute.snapshot.params['id'];
    this.getqueryParam = {"id":this.edit_id};
    this._http.post(this.getSingleColor,this.getqueryParam).subscribe((response:any)=>{
      //console.log(JSON.stringify(response));
      this.record = response.record;
      ///console.log(this.record);
      this.old_color_name = this.record[0].name; 
    });
  }


  ngOnInit(): void {
  }



  form = new UntypedFormGroup({
    edit_id: new UntypedFormControl('', []), 
    name: new UntypedFormControl('', [Validators.required]), 
    
  });
  
  get f(){
    return this.form.controls;
  }

   

  
  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

  submit()
  {
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this.formValue = this.form.value;
      //console.log(this.formValue);
      this._http.post(this.editColorsSubmit,this.formValue).subscribe((response:any)=>{
          //console.log("response of api"+response);
          this.apiResponse = response;

          if(this.apiResponse.status == true)
          {
            setTimeout(() => {
                //window.location.href = this.base_url+"allColor";
                window.history.back();
              }, 2000); 
            
          }

           
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }

}
