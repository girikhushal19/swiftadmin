import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';



@Component({
  selector: 'app-admin-settings',
  templateUrl: './admin-settings.component.html',
  styleUrls: ['./admin-settings.component.css']
})
export class AdminSettingsComponent implements OnInit {
  token:any;formData:any;base_url:any;base_url_node:any;user_type:any;apiResponse:any;formValue:any;settingSubmit:any;getModel:any;allModelList:any;getSetting:any;allCategoryType:any;
  tax_percent_old:any;max_merchant_limit_old:any;default_shipping_old:any;max_delivery_distance_old:any;driver_support_mobile_old:any;driver_support_email_old:any;max_pickup_distance_old:any;  getReturnPriceAPI=""; getReturnPrice:number=0; getProductTax:number=0; getShippingCost:number=0;
  getImidiateShippingCost:number=0; getCommission:number=0;
  getEstimateTime:number=0;
  admin_commission_old:any;return_max_day_old:any; getReturnPriceData:any; base_url_node_plan:string="";
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  {
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_plan = this.loginAuthObj.base_url_node;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.settingSubmit = this.base_url_node+"settingSubmit";
    //this.getModel = this.base_url_node+"getModel";
    //this.getSetting = this.base_url_node+"getSetting";

     
    //console.log("here");
    this.getReturnPrice = 0;
    this._http.get( this.base_url_node_plan+"admin-panel/getglobalsettings" ).subscribe((response:any)=>{
      this.apiResponse = response;
      //this.allCategoryType = response.result;
      console.log("response of api apiResponse ",this.apiResponse ); 
      if(this.apiResponse.status)
      {
        this.allCategoryType = this.apiResponse.data;

        this.return_max_day_old = this.allCategoryType.return_max_day;
        this.getReturnPrice = this.allCategoryType.returnprice;
        this.getProductTax = this.allCategoryType.tax;
        this.getShippingCost = this.allCategoryType.shipping_cost;
        this.getImidiateShippingCost = this.allCategoryType.imidiate_shipping_cost;
        this.getEstimateTime = this.allCategoryType.estimate_time;
        this.getCommission = this.allCategoryType.commission;
      }
      //console.log("response of api"+JSON.stringify(this.allCategoryType));
      //this.apiResponse = response;    
      /*
      for(let i=0; i<this.allCategoryType.length; i++)
      {
        if(this.allCategoryType[i].attribute_key == "tax_percent")
        {
          this.tax_percent_old = this.allCategoryType[i].attribute_value;
        }
        if(this.allCategoryType[i].attribute_key == "max_merchant_limit")
        {
          this.max_merchant_limit_old = this.allCategoryType[i].attribute_value;
        }
        if(this.allCategoryType[i].attribute_key == "return_max_day")
        {
          this.return_max_day_old = this.allCategoryType[i].attribute_value;
        }
      }
      */
    });
    
  }
  ngOnInit(): void {}

  form = new UntypedFormGroup({
    tax: new UntypedFormControl('', [Validators.required]),
    commission: new UntypedFormControl('', [Validators.required]),
    return_max_day: new UntypedFormControl('', [Validators.required]),
    returnprice: new UntypedFormControl('', [Validators.required]),
    shipping_cost: new UntypedFormControl('', [Validators.required]),
    imidiate_shipping_cost: new UntypedFormControl('', [Validators.required]),
    estimate_time: new UntypedFormControl('', [Validators.required])


  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  
  submit()
  {
    
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {

      // this._http.post(this.base_url_node_plan+"admin-panel/updateoraddSettings",this.form.value).subscribe((response:any)=>{
      //   //console.log("response of api"+response);
      //   this.apiResponse = response;
      //   if(this.apiResponse.error == false)
      //   {
      //     // setTimeout(() => {
      //     //   window.location.reload();
      //     // }, 2000); 
      //   }
      // });


      this._http.post(this.settingSubmit,this.form.value).subscribe((response:any)=>{
        //console.log("response of api"+response);
        this.apiResponse = response;
        if(this.apiResponse.error == false)
        {
          setTimeout(() => {
            window.location.reload();
          }, 2000); 
        }
      });
      
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}
