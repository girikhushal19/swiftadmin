import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-edit-zone-name',
  templateUrl: './edit-zone-name.component.html',
  styleUrls: ['./edit-zone-name.component.css']
})
export class EditZoneNameComponent implements OnInit {
   
  imageSrc: string = '';
  fileInputLabel: string = "";
  myFiles:string [] = [];
  base_url = "";base_url_node = ""; base_url_node_only = ""; 
  token:any;formData:any;images:any;video:any;user_type:any;apiResponse:any;formValue:any;editPaypalSubmit:any;getModel:any;allModelList:any;getCarCategory:any;allCarCategory:any;getPaypal:any;getqueryParam:any;removeQueryParam:any;old_category:any;old_price:any;old_images:any;record:any;removeCategoryImage:any;edit_id:string=""; old_name:string="";
  old_id:string="";
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  {

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    //console.log("this.base_url"+this.base_url);
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.editPaypalSubmit = this.base_url_node+"shipping/updateZoneName";
    //this.getCarCategory = this.base_url_node+"getCarCategory";
    //console.log("here");




    this.edit_id = this.actRoute.snapshot.params['id'];
    console.log(this.edit_id);
    this.getqueryParam = {};
    this.getPaypal = this.base_url_node+"shipping/getZonesByZoneId/"; 
    this._http.get(this.getPaypal+this.edit_id).subscribe((response:any)=>{
    console.log("getPaypal"+ JSON.stringify(response));
    //this.apiResponse = response;
      //this.record = response.record;
      //console.log(this.record); 
       if(response.status == true)
       {
        this.record = response.data;
        this.old_name = this.record.name

        this.old_id = this.record._id
       }
    });

   }

  ngOnInit(): void {
       
  }

  form = new UntypedFormGroup({
    id: new UntypedFormControl('', [ ]),
    zone_name: new UntypedFormControl('', [Validators.required]),
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
   

   

  submit()
  {
    
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      
        console.log(this.form.value); 

      this._http.post(this.editPaypalSubmit,this.form.value).subscribe((response:any)=>{
          console.log("response of api"+response);
          this.apiResponse = response;
          if(this.apiResponse.status == true)
          {
            setTimeout(() => {
                window.location.href = this.base_url+"shipping_area";
            }, 2000); 
          }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }


}
