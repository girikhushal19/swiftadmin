import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllExpiredEventComponent } from './all-expired-event.component';

describe('AllExpiredEventComponent', () => {
  let component: AllExpiredEventComponent;
  let fixture: ComponentFixture<AllExpiredEventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllExpiredEventComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllExpiredEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
