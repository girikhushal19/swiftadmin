import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Editor } from 'ngx-editor';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-seller-terms-condition',
  templateUrl: './seller-terms-condition.component.html',
  styleUrls: ['./seller-terms-condition.component.css']
})
export class SellerTermsConditionComponent implements OnInit {

  editor: Editor;
  html: any;
  base_url = "";base_url_node = "";
  token:any;user_type:any;apiResponse:any;formValue:any;editPageSubmit:any;getCarAttribute:any;getCarCategory:any;allCarAttribute:any;allCarCategory:any;edit_id:any;getSinglePage:any;getqueryParam:any;record:any;old_slug_name:any;old_page_title:any;old_description:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.editPageSubmit = this.base_url_node+"editSellerTermsCondition";
    this.getSinglePage = this.base_url_node+"getSingleSellerTermsConditionPage";
     
    //console.log("here");
     this.editor = new Editor();
     this.html = "";  

     
    this.getqueryParam = {"id":this.edit_id};
    this._http.get(this.getSinglePage).subscribe((response:any)=>{
      //console.log("getSingleModel"+JSON.stringify(response));
      this.record = response.record;

      this.edit_id = this.record[0]._id;
      this.old_description = this.record[0].description;
       /* console.log("edit_id "+this.edit_id)
        console.log("old_description "+this.old_description)*/

    });
  }


  ngOnInit(): void {
    this.editor = new Editor();
  }
   ngOnDestroy(): void {
    this.editor.destroy();
  }


  form = new UntypedFormGroup({
    edit_id: new UntypedFormControl('', []), 
    html: new UntypedFormControl('', [Validators.required]), 
  });
  
  get f(){
    return this.form.controls;
  }

   

  
  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

  submit()
  {
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this.formValue = this.form.value;
      console.log(this.formValue);
      this._http.post(this.editPageSubmit,this.formValue).subscribe((response:any)=>{
          //console.log("response of api"+response);
          this.apiResponse = response;

          if(this.apiResponse.error == false)
          {
             setTimeout(() => {
                //window.location.href = this.base_url+"allNormalUsers";
                window.location.reload();
              }, 2000);
          }

           
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }

}
