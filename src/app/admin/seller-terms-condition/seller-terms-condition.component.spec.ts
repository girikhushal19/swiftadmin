import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerTermsConditionComponent } from './seller-terms-condition.component';

describe('SellerTermsConditionComponent', () => {
  let component: SellerTermsConditionComponent;
  let fixture: ComponentFixture<SellerTermsConditionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SellerTermsConditionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SellerTermsConditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
