import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InActiveSellerComponent } from './in-active-seller.component';

describe('InActiveSellerComponent', () => {
  let component: InActiveSellerComponent;
  let fixture: ComponentFixture<InActiveSellerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InActiveSellerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(InActiveSellerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
