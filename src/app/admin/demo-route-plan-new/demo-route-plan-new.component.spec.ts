import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoRoutePlanNewComponent } from './demo-route-plan-new.component';

describe('DemoRoutePlanNewComponent', () => {
  let component: DemoRoutePlanNewComponent;
  let fixture: ComponentFixture<DemoRoutePlanNewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DemoRoutePlanNewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DemoRoutePlanNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
