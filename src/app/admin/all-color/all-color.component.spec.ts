import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllColorComponent } from './all-color.component';

describe('AllColorComponent', () => {
  let component: AllColorComponent;
  let fixture: ComponentFixture<AllColorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllColorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllColorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
