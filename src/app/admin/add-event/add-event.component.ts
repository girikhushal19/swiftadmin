import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators,FormArray, FormBuilder,FormGroup} from '@angular/forms';
import { Editor } from 'ngx-editor';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common'; 

@Component({
  selector: 'app-add-event',
  templateUrl: './add-event.component.html',
  styleUrls: ['./add-event.component.css']
})
export class AddEventComponent implements OnInit {
  myFiles:string [] = [];
  editor: Editor;
  html: any;
  imageSrc: string = ''; 
  fileInputLabel: string = "";
  base_url = "";base_url_node = ""; 
  token:any;formData:any;images:any;video:any;user_type:any;apiResponse:any;formValue:any;shippingSubmit:any;getCategory:any;allModelList:any;getCarCategory:any;allCarCategory:any;getSingleUser:any;getqueryParam:any;record:any;edit_id:any;base_url_node_only:any;
  zero_value:any;allCat:any;allOrgniser:any;
  productForm: UntypedFormGroup; dd:any;mm:any;
  startAddress: string = '';startLatitude: string = '';startLongitude: string = '';
  constructor(private fb:FormBuilder, private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  {
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.base_url_node = this.loginAuthObj.baseapiurl2;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }

    this.shippingSubmit = this.base_url_node+"api/event/createevent";

    this._http.get(this.base_url_node+"api/event/getallecatagory",{}).subscribe((response:any)=>{
      this.apiResponse = response;
      if(this.apiResponse.status == true)
      {
        this.allCat = this.apiResponse.data;
        this.apiResponse = {};
      }
    });
    this._http.get(this.base_url_node+"api/event/getallorganizer",{}).subscribe((response:any)=>{
      this.apiResponse = response;
      if(this.apiResponse.status == true)
      {
        this.allOrgniser = this.apiResponse.data;
        this.apiResponse = {};
      }
    });

    this.productForm = this.fb.group({
      title: new UntypedFormControl('',[Validators.required]),
      catagory: new UntypedFormControl('',[Validators.required]),
      totoal_ticket:  new UntypedFormControl('',[Validators.required]),
      orgniser: new UntypedFormControl('',[Validators.required]),
      paidornot: new UntypedFormControl('',[Validators.required]),
      startAddress: new UntypedFormControl('', [Validators.required]),
      start_date: new UntypedFormControl('', [Validators.required]),
      start_time: new UntypedFormControl('', [Validators.required]),
      end_date: new UntypedFormControl('', [Validators.required]),
      end_time: new UntypedFormControl('', [Validators.required]),
      description: new UntypedFormControl('', [Validators.required]),
      images: new UntypedFormControl('', [Validators.required]),
      quantities: this.fb.array([]) ,
      quantitiesMonday: this.fb.array([]) ,
    });
    this.editor = new Editor();
    this.html = ""; 
  }
  ngOnInit(): void {
    this.myFiles = []; 
  }

  ngOnDestroy(): void
  {
    this.editor.destroy();
  }
  onFileChange(event:any)
  {
    this.myFiles = [];
      for (var i = 0; i < event.target.files.length; i++)
      { 
          this.myFiles.push(event.target.files[i]);
      }
  }

  handleAddressChange(address: any)
  {
    //console.log(address);
    this.startAddress = address.formatted_address
    this.startLatitude = address.geometry.location.lat()
    this.startLongitude = address.geometry.location.lng()
  }
  get f(){
    return this.productForm.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  
  quantities() : FormArray {
    return this.productForm.get("quantities") as FormArray
  }
  quantitiesMonday() : FormArray {
    return this.productForm.get("quantitiesMonday") as FormArray
  }
  newQuantity(): FormGroup {
    return this.fb.group({

      tags: '',
    })
  }
  
  newQuantity2(): FormGroup {
    return this.fb.group({
      ticket_type: '',
      ticket_price: '',
      number_of_ticket: '',
    })
  }
  addQuantity() {
    this.quantities().push(this.newQuantity());
  }
  addQuantityMonday() {
    this.quantitiesMonday().push(this.newQuantity2());
  }
  removeQuantity(i:number) {
    this.quantities().removeAt(i);
  }
  removeQuantityMonday(i:number) {
    this.quantitiesMonday().removeAt(i);
  }
  onSubmit() {
    if(this.productForm.valid)
    {
      // "tickets":[
      //   {"type":"one","price":100,"nooftickets":50},
      //   {"type":"two","price":200,"nooftickets":20}
      // ],

      console.log(this.productForm.value);
      
      var today_date = new Date(this.productForm.value.start_date);
      const yyyy = today_date.getFullYear();
      this.mm = today_date.getMonth() + 1; // Months start at 0!
      this.dd = today_date.getDate();

      if (this.dd < 10)
      {
        this.dd = '0' + this.dd;
      }
      if (this.mm < 10)
      {
        this.mm = '0' + this.mm;
      } 
      const formatted_start_date = yyyy + '-' + this.mm + '-' + this.dd;
      //console.log("formatted_start_date "+formatted_start_date);

      var today = new Date(this.productForm.value.start_time);
      var h = today.getHours();
      var m = today.getMinutes();
      let start_time_final = h+"h"+m;
      //console.log("h "+h +"   "+"m "+ m );

      var dateandtime = [];
      let obj_start_date = {date:formatted_start_date,time:start_time_final};
      dateandtime.push(obj_start_date);
      var today_date = new Date(this.productForm.value.end_date);
      const yyyy2 = today_date.getFullYear();
      this.mm = today_date.getMonth() + 1; // Months start at 0!
      this.dd = today_date.getDate();

      if (this.dd < 10)
      {
        this.dd = '0' + this.dd;
      }
      if (this.mm < 10)
      {
        this.mm = '0' + this.mm;
      } 
      const formatted_end_date = yyyy2 + '-' + this.mm + '-' + this.dd;
      //console.log("formatted_end_date "+formatted_end_date);

      var today = new Date(this.productForm.value.end_time);
      var h = today.getHours();
      var m = today.getMinutes();
      //console.log("h "+h +"   "+"m "+ m );
      let end_time_final = h+"h"+m;

      let obj_end_date = {date:formatted_end_date,time:end_time_final};
      dateandtime.push(obj_end_date);
      //console.log("dateandtime   "+dateandtime);

      var org_id;var org_name;
      var org = this.productForm.value.orgniser;
      var arrOrg = org.split(",");
      if(arrOrg.length > 0)
      {
        // console.log("arrOrg " +arrOrg[0]);
        // console.log("arrOrg " +arrOrg[1]);
        org_id = arrOrg[0];
        org_name = arrOrg[1];
      }
      var tick = this.productForm.value.quantitiesMonday;
      var tickArray = [];
      for(let x=0; x<tick.length; x++)
      {
        let obja = {"type":tick[x].ticket_type,"price":tick[x].ticket_price,"nooftickets":tick[x].number_of_ticket};
        tickArray.push(obja);
      }
      //console.log("tickArray "+tickArray);

      var tagg = this.productForm.value.quantities;
      var tagArray = [];
      for(let x=0; x<tagg.length; x++)
      {
        tagArray.push(tagg[x].tags);
      }
      //console.log("tagArray"+tagArray);
      let addrObj = {
        "address":this.startAddress,
        "latlong":[this.startLatitude,this.startLongitude]
      };
      let queryParam = {
        "dateandtime":dateandtime,
        "paidornot":this.productForm.value.paidornot,
        "title":this.productForm.value.title,
        "org_id":org_id,
        "org_name":org_name,
        "catagory":this.productForm.value.catagory,
        "tags":tagArray,
        "tickets":tickArray,
        "address":addrObj,
        "description":this.productForm.value.description,
      };

      this.formData = new FormData(); 
      this.formData.append("dateandtime",JSON.stringify(dateandtime)); 
      this.formData.append("paidornot",this.productForm.value.paidornot);
      this.formData.append("title",this.productForm.value.title); 
      this.formData.append("org_id",org_id); 
      this.formData.append("org_name",org_name); 
      this.formData.append("catagory",this.productForm.value.catagory); 
      this.formData.append("tags",JSON.stringify(tagArray)); 
      this.formData.append("tickets",JSON.stringify(tickArray)); 
      this.formData.append("address",JSON.stringify(addrObj)); 
      this.formData.append("description",this.productForm.value.description);
      this.formData.append("totoal_ticket",this.productForm.value.totoal_ticket);
      
      for (var i = 0; i < this.myFiles.length; i++)
      { 
        this.formData.append("photo", this.myFiles[i]);
      }

      console.log("queryParam ->>>>>> "+JSON.stringify(queryParam));
      this._http.post(this.shippingSubmit,this.formData).subscribe((response:any)=>{
          console.log("response of api"+response);
          this.apiResponse = response;
        if(this.apiResponse.error == false)
        {
          console.log("here");
          setTimeout(() => {
              window.location.reload();
          }, 2000); 
        }
      });
    }else{
      console.log('erro form submitted');
      this.validateAllFormFields(this.productForm); 
      // validate all form fields
    }

  }


} 
