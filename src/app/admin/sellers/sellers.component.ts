import { Component, OnInit } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { EditvendorComponent } from '../editvendor/editvendor.component';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
@Component({
  selector: 'app-sellers',
  templateUrl: './sellers.component.html',
  styleUrls: ['./sellers.component.css']
})
export class SellersComponent implements OnInit {
  searchText:any;
  page_no:number=0;numbers:any;selectedIndex: number=0;
  base_url:any;
  vendorsarray:any;base_url_two:string="";apiResponse:any;
  vendors:any;deletemultipleseller:string="";
  invoice_id:any[] = [];
  constructor(private _http:HttpClient,public dialog: MatDialog,private loginAuthObj:LoginauthenticationService,private http: HttpClient) {
    this.base_url = loginAuthObj.baseapiurl
    this.vendorsarray =[]
    this.vendors = []
    this.base_url_two = loginAuthObj.baseapiurl2;
    this.deletemultipleseller = this.base_url_two+"api/seller/deletemultipleseller";

    let queryParam1 = {pageno:null};
    this.http.post(this.base_url+"vendors",queryParam1).subscribe(res => {
      this.vendorsarray = res
      //this.vendors =this.vendorsarray.data
      if(this.vendorsarray.status == true)
      {
        this.page_no = this.vendorsarray.pages;
        this.numbers = Array(this.page_no).fill(0).map((m,n)=>n);
        console.log("this.numbers ---->  "+this.numbers);
      }
    })

    this.getallColor(0)
   }
   getallColor(pageno:number=0)
   {
      this.selectedIndex = pageno;
      let queryParam = {pageno:pageno};
      this.http.post(this.base_url+"vendors",queryParam).subscribe(res => {
        this.vendorsarray = res
        this.vendors =this.vendorsarray.data
        //console.log(this.orders)
      })
   }
  ngOnInit(): void {
    

  }

  select_all = false;
  data: any[] = [
  ]

  onSelectAll(e: any): void { 
    //console.log("is checked "+e);
    for (let i = 0; i < this.vendors.length; i++)
     {
       const item = this.vendors[i];
       item.is_checked = e;
       
     }
 
    if(e == true)
    {
     for (let i = 0; i < this.vendors.length; i++)
     {
       const item = this.vendors[i];
       item.is_checked = e;
       //console.log(item._id);
       let cc = item._id;
       this.invoice_id.push(cc);
     }
    }else{
     this.invoice_id = [];
    }
    console.log("invoiceeee "+this.invoice_id);
   }
   onSelectAll2(id:any,event:any)
   {
     console.log("id"+id);
     console.log("event"+event);
     if(event == true)
     {
       this.invoice_id.push(id);
     }else{
       const index = this.invoice_id.indexOf(id);
       this.invoice_id.splice(index, 1)
     }
 
     console.log("invoiceeee single select"+this.invoice_id);
   }
  openDialog(): void {
    const dialogRef = this.dialog.open(EditvendorComponent ,{
      height: '400px',
      width: '700px'
    });
  }
  deleteUser(id:any){
    console.log(id)
      if(confirm("Voulez-vous supprimer cet utilisateur ?") == true){
        this.http.get(this.base_url+"deletevendor/"+id).subscribe(res => {
          // alert("user deleted successfully")
          // console.log(res)

          this.apiResponse= res;
          if(this.apiResponse.status == true)
          {
            setTimeout(() => {
              window.location.reload();
            }, 1000); 
          }


        })
      }
  }

  form = new UntypedFormGroup({
    action_delete: new UntypedFormControl('', [])
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  submit()
  {
    console.log(this.form.value);
    if(this.form.value.action_delete == "delete")
    {
      let queryParam = {"ids":this.invoice_id};
      this._http.post(this.deletemultipleseller,queryParam).subscribe((response:any)=>{
        console.log("response"+response);
        this.apiResponse= response;
        if(this.apiResponse.status == true)
        {
          setTimeout(() => {
            window.location.reload();
          }, 1000); 
        }
      });
    }
  }

  inactiveUser(id:any)
  {
    console.log(id);
    //{{local}}/admin-panel//63bd435dee7014ce11fe2cf3 this.base_url_two+"api/seller/
    this.http.post(this.base_url_two+"api/seller/markvendorasInactive/",{id:id}).subscribe(res => {
      this.apiResponse = res;
      //alert("user deleted successfully")
      console.log(this.apiResponse)
      console.log("status "+this.apiResponse.status)
      if(this.apiResponse.status == true)
      {
        setTimeout(() => {
          window.location.reload();
        }, 1000); 
      }
      //console.log(res)
    });
  }
  activeUser(id:any)
  {
    console.log(id);
    this.http.post(this.base_url_two+"api/seller/markvendorasactive/",{id:id}).subscribe(res => {
      this.apiResponse = res;
      //alert("user deleted successfully")
      console.log(this.apiResponse)
      console.log("status "+this.apiResponse.status)
      if(this.apiResponse.status == true)
      {
        setTimeout(() => {
          window.location.reload();
        }, 1000); 
      }
    });
  }

}
