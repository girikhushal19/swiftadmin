import { Component, OnInit,HostListener, ViewChild, ElementRef,AfterViewInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { IDropdownSettings } from 'ng-multiselect-dropdown';

@Component({
  selector: 'app-add-route',
  templateUrl: './add-route.component.html',
  styleUrls: ['./add-route.component.css']
})
export class AddRouteComponent implements OnInit {

  dropdownList : any[] = [];
  dropdownListNew : any[] = [];
  selectedItems : any[] = [];
   
     dropdownSettings = {};

  stop_type_for_user:string="";
  merchant_display_val:string = "none";
  user_display_val:string = "none";
  markers:any[] = [];
  position:any;
  @ViewChild('map', {static: false}) mapElement:any= ElementRef;
  map:any;
  marker:any;
  directionsRendererccc:any;
  center = new google.maps.LatLng(48.8592877,2.3310465);
  html_view:string="";
  driver_id:string="";
  routeRecord:any;driverRecord:any;newObject:object={};
  getqueryParam:any;deleteStopApi:any
  imageSrc: string = '';
  fileInputLabel: string = "";
  myFiles:string [] = [];
  base_url = "";base_url_node = ""; queryParam:any;allStops:any;
  token:any;formData:any;images:any;video:any;user_type:any;apiResponse:any;formValue:any;addRouteSubmit:any;getModel:any;allModelList:any;getAllRouteStop:any;getRouteAssignCheck:any; allCarCategory:any;getAdminAllShipmentId: string = "";allShipmentId:any;
  all_user_order_Id:any;
  addRouteFileSubmit:any;route_id:any;
  default_date: Date = new Date(); default_date2: Date = new Date();
  default_date3: Date = new Date(); default_date4: Date = new Date();
  //ddDate = new Date('Thu Jan 26 2023 22:38:32 GMT+0530 (India Standard Time)');
  address: string = '';latitude: string = '';longitude: string = '';
  endAddress: string = '';endLatitude: string = '';endLongitude: string = '';
  allFormValue:any;getStopList:any;getAdminShipmentAddress: string = '';record:any;
  numericArray: number [] = [];
  Normal:string = "Normal";
  startPoint: string = '';endPoint: string = '';base_url_two: string = '';
  displayStyle = "none";
  displayStyle2 = "none";
  openPopup() {
    this.displayStyle = "block";
  }
  closePopup() {
    this.displayStyle = "none";
  }

  openPopup2() {
    this.displayStyle2 = "block";
  }
  closePopup2() {
    this.displayStyle2 = "none";
  }
  @HostListener('paste', ['$event']) blockPaste(e: KeyboardEvent) {
    e.preventDefault();
  }

  @HostListener('copy', ['$event']) blockCopy(e: KeyboardEvent) {
    e.preventDefault();
  }

  @HostListener('cut', ['$event']) blockCut(e: KeyboardEvent) {
    e.preventDefault();
  }
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_two = this.loginAuthObj.baseapiurl2
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.addRouteSubmit = this.base_url_node+"addRouteSubmit";
    this.addRouteFileSubmit = this.base_url_node+"addRouteFileSubmit";
    this.getRouteAssignCheck = this.base_url_node+"getRouteAssignCheck";
    this.getAllRouteStop = this.base_url_node+"getAllRouteStop";
    this.getAdminAllShipmentId = this.base_url_node+"getAdminAllShipmentId";
    this.deleteStopApi = this.base_url_node+"deleteStopApi";
    this.getAdminShipmentAddress = this.base_url_node+"getAdminShipmentAddress";
    
    //console.log("here");
    for(let x=1; x<=60; x++)
    {
      this.numericArray.push(x);
    }
    this.route_id = this.actRoute.snapshot.params['id'];
    marker: google.maps.Marker;

    if(this.route_id != undefined || this.route_id !=  "" || this.route_id != null)
      {
        console.log("this.route_id ifffffffff  --->>>>>>>>>> "+this.route_id);
        this.queryParam = {"route_id":this.route_id};
        this._http.post(this.getRouteAssignCheck,this.queryParam).subscribe((response:any)=>{
          console.log("response   --->>>> "+ JSON.stringify(response));
            if(response.error == false)
            {
              this.driver_id = response.record.driver_id[0];
              console.log("this.driver_id   120 ----->   "+this.driver_id);
            }
          });
      }
  }
  ngOnInit(): void {

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Sélectionner tout',
      unSelectAllText: 'Ne pas sélectionner tout',
      itemsShowLimit: 10,
      allowSeachFilter: true
   } 


      this.myFiles = []; 
      // console.log(this.ddDate);
      // console.log(this.ddDate.getHours());
      // console.log(this.ddDate.getMinutes());
      // console.log(this.ddDate.getSeconds());
      //this._http.post(this.addRouteFileSubmit,this.formData).subscribe((response:any)=>{
      this.queryParam = {"route_id":this.route_id};
      this._http.post(this.getAllRouteStop,this.queryParam).subscribe((response:any)=>{
        if(response.error == false)
        {
          this.allStops = response.routeRecord;
          console.log("this.allStops "+this.allStops);

          this.markers = this.allStops;
          if(this.allStops != "")
          {
            console.log("ifffffffffffffffff 151 " );
            this.abcFun();
          }
          
        }
      });
      this._http.post(this.getAdminAllShipmentId,{}).subscribe((response:any)=>{
        if(response.error == false)
        {
          //console.log("response --> "+response);
          this.allShipmentId = response.record;
        }
      });
      
      this._http.get(this.base_url_two+"api/order/getallreadytopickuporderparts").subscribe((response:any)=>{
        if(response.status == true)
        {
          //console.log("response --> "+response);
          this.all_user_order_Id = response.data;
          //console.log("all_user_order_Id ->>>.  "+ JSON.stringify(this.all_user_order_Id));

          var abc = [];
          for (var _i = 0; _i < this.all_user_order_Id.length; _i++)
          {
            abc.push(
              {item_id: this.all_user_order_Id[_i]._id+","+this.all_user_order_Id[_i].order_id+","+this.all_user_order_Id[_i].index,item_text: this.all_user_order_Id[_i].order_id}
              );
            this.dropdownListNew = abc;
            //console.log(this.dropdownListNew);
            //this.dropdownListNew[_i] = { item_id: this.record[_i]._id, item_text: this.record[_i].firstName+" "+this.record[_i].lastName };
          }
        }
      });
      
      
  }
  onItemSelect(item: any) {
    console.log("here 185 ->>>>> "+JSON.stringify(item));
    console.log("here 185 ->>>>> "+item.item_id);
    let user_order_id_let = item.item_id;
    if(user_order_id_let)
    {
      let stopType22 = this.form.value.stopType2;
      this.getAddressFunctionUserOrder(stopType22,user_order_id_let)
    }
  }
  onSelectAll(items: any) {
    console.log(items);
  }

  onChangeR(event:any)
  {
    console.log("value "+event);
    console.log("value stopType "+this.form.value.stopType);
    this.getAddressFunction(this.form.value.stopType,event)
  }
  onChangeR2(event:any)
  {
    console.log("value "+event);
    console.log("value merchant_order_id "+this.form.value.merchant_order_id);
    this.getAddressFunction(event,this.form.value.merchant_order_id)
  }
  orderTypeFun(event:any)
  {
    console.log("value order type --> "+event);
    if(event == 'user_order')
    {
      this.merchant_display_val = "none";
      this.user_display_val = "block";
    }else{
      this.merchant_display_val = "block";
      this.user_display_val = "none";
    }
    this.stop_type_for_user = this.form.value.stopType2;
    console.log("this.stop_type_for_user ---->>>>>  "+this.stop_type_for_user);
    
  }
  onChangeUserOrder(event:any)
  { 
    //console.log("value  user order ID --> "+event);
    let stopType22 = this.form.value.stopType2;
    let user_order_id_let = this.form.value.user_order_id;
    this.getAddressFunctionUserOrder(stopType22,user_order_id_let)
  }
  onChangeRUserType(event:any)
  {
    this.stop_type_for_user = this.form.value.stopType2;
    this.address = "";
    this.latitude = "";
    this.longitude = "";

    let stopType22 = this.form.value.stopType2;

    if(this.stop_type_for_user == "Delivery")
    {
      if(this.form.value.user_order_id_arr)
      {
        var user_order_id_let = this.form.value.user_order_id_arr;
        //console.log("this.user_order_id_let 242 ---->>>>>  "+ JSON.stringify(user_order_id_let));
        //console.log("this.user_order_id_let 242 ---->>>>>  "+ JSON.stringify(user_order_id_let[0].item_id));
        var user_order_id_var = user_order_id_let[0].item_id;
      }
    }else{
      var user_order_id_var = this.form.value.user_order_id;
    }
    if(stopType22 && user_order_id_var)
    {
      this.getAddressFunctionUserOrder(stopType22,user_order_id_var)
    }
    
  }
  getAddressFunctionUserOrder(stopType:string='',user_order_id:any)
  {
    console.log("user_order_id  253 ---> >>> "+JSON.stringify(user_order_id));
    console.log("stopType  ---> >>> "+stopType);
    if(user_order_id)
    {
      var arr_user_order_id = user_order_id.split(",");
      if(arr_user_order_id)
      {
        if(arr_user_order_id.length > 2)
        {
          // console.log("arr_user_order_id ", arr_user_order_id); 
          // console.log("arr_user_order_id ", arr_user_order_id[1]); 

          let ext_part_id = arr_user_order_id[1].split("_");


          console.log("ext_part_id ", ext_part_id); 
          if(ext_part_id.length > 1)
          {
            let part_id = ext_part_id[1];

            this.queryParam = {"type":stopType,"order_id":arr_user_order_id[2],"id":arr_user_order_id[0],part_id:part_id};
            this._http.post(this.base_url_two+"api/order/getaddressbypartid",this.queryParam).subscribe((response:any)=>{
              console.log(response);
              if(response.status == true)
              {
                this.record = response.data;
                if(this.record)
                {
                  if(this.record.address)
                  {
                    if(this.record.address.latlong.length > 0)
                    {
                      this.address = this.record.address.address;
                      this.latitude = this.record.address.latlong[0];
                      this.longitude = this.record.address.latlong[1];
                      console.log("this.address ->>>>> "+this.address);
                      console.log("this.latitude ->>>>> "+this.latitude);
                      console.log("this.longitude ->>>>> "+this.longitude);
                    }
                  }
                }
                //{"status":true,"data":{"order_id":"1020-1","_id":"63fddc76e973b1f5936615f6","address":{"latlong":[24,30],"address":"some place nice","_id":"64131c7e65f569458cb5e199"}}}

              
              }
            });
          }

          
        }
      }
    }
    
  }
  getAddressFunction(stopType:string='',merchant_order_id:any)
  {
    if(stopType != "" && stopType != 'undefined' && merchant_order_id != "" && merchant_order_id != 'undefined')
    {
      this.queryParam = {"stopType":stopType,"shipment_id":merchant_order_id};
      this._http.post(this.getAdminShipmentAddress,this.queryParam).subscribe((response:any)=>{
        console.log(response);
        if(response.error == false)
        {
          this.record = response.record;
          if(this.record)
          {
            if(stopType == 'Pickup')
            {
              //console.log(this.record.pickup_address);
              if(this.record.pickup_location)
              {
                if(this.record.pickup_location.coordinates)
                {
                  if(this.record.pickup_location.coordinates.length>1)
                  {
                    // console.log(this.record.pickup_location.coordinates[0]);
                    // console.log(this.record.pickup_location.coordinates[1]);
                    this.address = this.record.pickup_address;
                    this.latitude = this.record.pickup_location.coordinates[0];
                    this.longitude = this.record.pickup_location.coordinates[1];
                  }
                }
              }

            }else if(stopType == 'Delivery')
            {
              console.log(this.record);
              if(this.record.receiver_location)
              {
                if(this.record.receiver_location.coordinates)
                {
                  if(this.record.receiver_location.coordinates.length>1)
                  {
                    // console.log(this.record.receiver_location.coordinates[0]);
                    // console.log(this.record.receiver_location.coordinates[1]);
                    this.address = this.record.receiver_address;
                    this.latitude = this.record.receiver_location.coordinates[0];
                    this.longitude = this.record.receiver_location.coordinates[1];
                  }
                }
              }
            }
          }
        }
      });
    }
  }

  handleAddressChange(address: any)
  {
    //console.log(address);
    this.address = address.formatted_address
    this.latitude = address.geometry.location.lat()
    this.longitude = address.geometry.location.lng()
  }

  form = new UntypedFormGroup({
    orderType: new UntypedFormControl('', [Validators.required]),
     
    merchant_order_id: new UntypedFormControl('', []),
    user_order_id: new UntypedFormControl('', []),
    user_order_id_arr: new UntypedFormControl('', []),

    stopType: new UntypedFormControl('', []),
    stopType2: new UntypedFormControl('', []),
    
    fullName: new UntypedFormControl('', []),
    mobileNumber: new UntypedFormControl('', []),
    notes: new UntypedFormControl('', []),
    priority: new UntypedFormControl('', [Validators.required]),
    serviceTime: new UntypedFormControl('', []),
    address: new UntypedFormControl('', [Validators.required]),
    businessName: new UntypedFormControl('', []),
    email: new UntypedFormControl('', []),
    parcelFront: new UntypedFormControl('', []),
    parcelLeft: new UntypedFormControl('', []),
    parcelFloor: new UntypedFormControl('', []),
    parcelCount: new UntypedFormControl('', []),
    images: new UntypedFormControl('', []),
    route_id: new UntypedFormControl('', []),
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  onFileChange(event:any)
  {
    this.myFiles = [];
      for (var i = 0; i < event.target.files.length; i++)
      { 
          this.myFiles.push(event.target.files[i]);
      }
  }
  submit()
  {
    this.apiResponse = {"error":false,"errorMessage":""};
    if(this.form.valid)
    {
      console.log("this.form ", this.form.value);
      console.log("this.form orderType ", this.form.value.orderType);
      //console.log(this.myFiles);
      if(this.form.value.orderType == "" || this.form.value.orderType == undefined || this.form.value.orderType == null)
      {
        this.apiResponse = {"error":true,"errorMessage":"Le type de commande est requis"};
        return;
      }

      if(this.form.value.orderType == "merchant_order")
      {
        if(this.form.value.stopType == "" || this.form.value.stopType == undefined || this.form.value.stopType == null)
        {
          this.apiResponse = {"error":true,"errorMessage":"Le type d'arrêt est requis"};
          return;
        }
      }else{
        if(this.form.value.stopType2 == "" || this.form.value.stopType2 == undefined || this.form.value.stopType2 == null)
        {
          this.apiResponse = {"error":true,"errorMessage":"Le type d'arrêt est requis"};
          return;
        }
      }
      
      console.log("this.form user_order_id_arr ", this.form.value.user_order_id_arr);

      if(this.form.value.user_order_id == "" && this.form.value.merchant_order_id == "" && this.form.value.user_order_id_arr.length == 0 )
      {
        this.apiResponse = {"error":true,"errorMessage":"L'identifiant de la commande est requis"};
        return;
      }

      console.log("form valid");
      this.formData = new FormData();

      if(this.myFiles.length > 0)
      {
        for (var i = 0; i < this.myFiles.length; i++)
        { 
          console.log(this.myFiles[i]);
          this.formData.append("file", this.myFiles[i]);
        } 
        this.formData.append('fullName', this.form.value.fullName); 
        this.formData.append('notes', this.form.value.notes); 
        this.formData.append('email', this.form.value.email); 
        this.formData.append('mobileNumber', this.form.value.mobileNumber); 
        this.formData.append('priority', this.form.value.priority);
        this.formData.append('serviceTime', this.form.value.serviceTime);
        this.formData.append('address', this.address);
        this.formData.append('businessName', this.form.value.businessName);
        this.formData.append('stopType', this.form.value.stopType);
        this.formData.append('stopType2', this.form.value.stopType2);
        this.formData.append('parcelFront', this.form.value.parcelFront); 
        this.formData.append('parcelLeft', this.form.value.parcelLeft); 
        this.formData.append('parcelFloor', this.form.value.parcelFloor); 
        this.formData.append('parcelCount', this.form.value.parcelCount); 
        this.formData.append('latitude', this.latitude); 
        this.formData.append('longitude', this.longitude); 
        this.formData.append('route_id', this.form.value.route_id); 
        this.formData.append('shipment_id', this.form.value.merchant_order_id); 
        this.formData.append('user_order_id', this.form.value.user_order_id);
        this.formData.append('user_order_id_arr', this.form.value.user_order_id_arr);
        
        
        this.formData.append('orderType', this.form.value.orderType); 
        

        console.log(this.formData);
        this._http.post(this.addRouteFileSubmit,this.formData).subscribe((response:any)=>{
          //console.log("response of api"+response);
          this.apiResponse = response;
          if(this.apiResponse.error == false)
          {
            
            //this.form.reset();
            //this.myFiles = [];
            setTimeout(() => {
              //window.location.reload();
              window.location.href = this.base_url+"addRoute/"+this.apiResponse.lastInsertId;
            }, 2000); 
          }
        });
        
      }else{
        this.allFormValue = {
          "shipment_id":this.form.value.merchant_order_id,
          "user_order_id":this.form.value.user_order_id,
          "user_order_id_arr":this.form.value.user_order_id_arr,
          "fullName":this.form.value.fullName,
          "email":this.form.value.email,
          "mobileNumber":this.form.value.mobileNumber,
          "notes":this.form.value.notes,
          "priority":this.form.value.priority,
          "serviceTime":this.form.value.serviceTime,
          "businessName":this.form.value.businessName,
          "stopType":this.form.value.stopType,
          "stopType2":this.form.value.stopType2,
          "parcelFront":this.form.value.parcelFront,
          "parcelLeft":this.form.value.parcelLeft,
          "parcelFloor":this.form.value.parcelFloor,
          "parcelCount":this.form.value.parcelCount,
          "route_id":this.form.value.route_id,
          "orderType":this.form.value.orderType,
          "address":this.address,
          "latitude":this.latitude,
          "longitude":this.longitude,
        };
        this._http.post(this.addRouteSubmit,this.allFormValue).subscribe((response:any)=>{
          //console.log("response of api"+response);
          this.apiResponse = response;
          if(this.apiResponse.error == false)
          {
            //this.form.reset();
            //this.myFiles = [];
            setTimeout(() => {
              //window.location.reload();
              window.location.href = this.base_url+"addRoute/"+this.apiResponse.lastInsertId;
            }, 2000); 
          }
        });
      }
      
    }else{
      console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }

  deleteStop(id:any)
  {
    console.log("id "+id);
    this.getqueryParam = {"id":id};
    this._http.post(this.deleteStopApi,this.getqueryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        setTimeout(() => {
          window.location.reload();
        }, 2000); 
      }
    });
  }
  abcFun(){

    this.mapInit();
    
    //console.log("this.markers "+this.markers);
    var lat_lng = new Array();
    var latlngbounds = new google.maps.LatLngBounds();
    for (let i = 0; i < this.markers.length; i++)
    {
      let m = i+1;
      let icon = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld='+m+'|333333|EA4335';

      var data = this.markers[i];
      console.log("data "+data.address);
      //console.log("data "+JSON.stringify(data.location.coordinates[0]));
      var myLatlng = new google.maps.LatLng(data.location.coordinates[0], data.location.coordinates[1]);
      lat_lng.push(myLatlng);
      console.log("myLatlng "+myLatlng);
      var mappppp = this.map;
      var marker = new google.maps.Marker({
        position: myLatlng,
        map: this.map,
        icon:icon,
        title:data.address,
      });
      var infoWindow = new google.maps.InfoWindow({
        content: "contentString",
        ariaLabel: "Uluru",
      });
      // console.log(i)
      //console.log("myLatlng "+myLatlng);
      latlngbounds.extend(myLatlng);
      (function(marker, data) {
        google.maps.event.addListener(marker, "click", function(e:any) {
          infoWindow.setContent(data.address);
          infoWindow.open(mappppp, marker);
        });
      })(marker, data);

      


    }
    //console.log(google.maps.DirectionsTravelMode); 
    //console.log("latlngbounds.getCenter() "+latlngbounds.getCenter());

    this.map.setCenter(latlngbounds.getCenter());
    this.map.fitBounds(latlngbounds);


    var service = new google.maps.DirectionsService();
    var cd = this.map;
    //var DRIVING = google.maps.DirectionsTravelMode.DRIVING;
      //Loop and Draw Path Route between the Points on MAP
      for (var i = 0; i < lat_lng.length; i++)
      {
        if ((i + 1) < lat_lng.length)
        {
          var src = lat_lng[i];
          var des = lat_lng[i + 1];
          // path.push(src);
          //console.log(des);
          service.route({
            origin: src,
            destination: des,
            travelMode: google.maps.TravelMode.DRIVING,
          }, function(result:any, status) {
            if (status == google.maps.DirectionsStatus.OK) {

              //Initialize the Path Array
              // var path = new google.maps.MVCArray();
              // //Set the Path Stroke Color
              // var poly = new google.maps.Polyline({
              //   map: cd,
              //   strokeColor: "#4A89F3",
              //   strokeOpacity: 1.0,
              //   strokeWeight: 8,
              // });
              // poly.setPath(path);
              // for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
              //   path.push(result.routes[0].overview_path[i]);
              // }
            }
          });
        }
      }
  }  
  
  mapInit()
  {
    
    this.map = new google.maps.Map(this.mapElement.nativeElement,
    this.mapOptions);
  }
  mapOptions: google.maps.MapOptions = {
    center: this.center,
    zoom: 2,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
}
