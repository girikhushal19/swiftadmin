import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';


@Component({
  selector: 'app-edit-drivers',
  templateUrl: './edit-drivers.component.html',
  styleUrls: ['./edit-drivers.component.css']
})
export class EditDriversComponent implements OnInit {
  imageSrc: string = '';
  fileInputLabel: string = "";
  myFiles:string [] = [];
  base_url = "";base_url_node = ""; 
  token:any;formData:any;images:any;video:any;user_type:any;apiResponse:any;formValue:any;editDriverSubmit:any;getSingleDriverApi:any;allModelList:any;getCarCategory:any;allCarCategory:any;
  edit_id:any;getqueryParam:any;record:any;
  default_date: Date = new Date(); default_date2: Date = new Date();
  default_date3: Date = new Date(); default_date4: Date = new Date();
  //ddDate = new Date('Thu Jan 26 2023 22:38:32 GMT+0530 (India Standard Time)');
  startAddress: string = '';startLatitude: string = '';startLongitude: string = '';
  endAddress: string = '';endLatitude: string = '';endLongitude: string = '';
  allFormValue:any;
  old_vehicleType:any;old_fullName:any;old_email:any;old_mobileNumber:any;old_id:any;
  old_shiftStartTime:any;old_shiftEndTime:any;old_breakStartTime:any;old_breakEndTime:any;
  old_startAddress:any;old_endAddress:any;old_startLocation:any;old_endLocation:any;cordinate:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.editDriverSubmit = this.base_url_node+"editDriverSubmit";
    this.getSingleDriverApi = this.base_url_node+"getSingleDriverApi";
    //this.getCarCategory = this.base_url_node+"getCarCategory";
    //console.log("here");
    this.edit_id = this.actRoute.snapshot.params['id'];
    this.getqueryParam = {id:this.edit_id};
    this._http.post(this.getSingleDriverApi,this.getqueryParam).subscribe((response:any)=>{
      //console.log("getSingleUser"+response);
      //this.apiResponse = response;
      if(response.error == false)
      {
        this.record = response.record;
      
        //console.log("record "+JSON.stringify(this.record)); 
        //console.log(this.record);
        
        this.old_vehicleType = this.record.vehicleType;
        this.old_fullName = this.record.fullName;
        this.old_email = this.record.email; 
        this.old_mobileNumber = this.record.mobileNumber; 
        this.old_id = this.record._id; 
        this.old_shiftStartTime = this.record.shiftStartTime;
        this.old_shiftEndTime = this.record.shiftEndTime;
        this.old_breakStartTime = this.record.breakStartTime;
        this.old_breakEndTime = this.record.breakEndTime;
        this.startAddress = this.record.startAddress;
        this.endAddress = this.record.endAddress;
        this.old_startLocation = this.record.startLocation;
        this.old_endLocation = this.record.endLocation;
        if(this.old_startLocation)
        {
          if(this.old_startLocation.coordinates)
          {
            ///console.log("old_endLocation "+this.old_startLocation.coordinates);
            if(this.old_startLocation.coordinates.length > 0)
            {
              this.startLatitude = this.old_startLocation.coordinates[0];
              this.startLongitude =  this.old_startLocation.coordinates[1];
              //  console.log("startLatitude"+this.startLatitude);
              //  console.log("startLongitude"+this.startLongitude);
            }
          }
        }
        if(this.old_endLocation)
        {
          if(this.old_endLocation.coordinates)
          {
            ///console.log("old_endLocation "+this.old_endLocation.coordinates);
            if(this.old_endLocation.coordinates.length > 0)
            {
              this.endLatitude = this.old_endLocation.coordinates[0];
              this.endLongitude =  this.old_endLocation.coordinates[1];
              //  console.log("endLatitude"+this.endLatitude);
              //  console.log("endLongitude"+this.endLongitude);
            }
          }
        }

      }  
       
  
        /*console.log(this.record._id); 
        console.log(this.record.price); */
        //console.log(this.old_category); 
      });
  }
  ngOnInit(): void {
      this.myFiles = []; 
      // console.log(this.ddDate);
      // console.log(this.ddDate.getHours());
      // console.log(this.ddDate.getMinutes());
      // console.log(this.ddDate.getSeconds());
  }

  handleAddressChange(address: any)
  {
    //console.log(address);
    this.startAddress = address.formatted_address
    this.startLatitude = address.geometry.location.lat()
    this.startLongitude = address.geometry.location.lng()
  }
  handleAddressChange2(address2: any)
  {
    //console.log(address2);
    this.endAddress = address2.formatted_address
    this.endLatitude = address2.geometry.location.lat()
    this.endLongitude = address2.geometry.location.lng()
  }

  form = new UntypedFormGroup({
    fullName: new UntypedFormControl('', [Validators.required]),
    email: new UntypedFormControl('', [Validators.required]),
    mobileNumber: new UntypedFormControl('', [Validators.required]),
    vehicleType: new UntypedFormControl('', [Validators.required]),
    shiftStartTime: new UntypedFormControl('', []),
    shiftEndTime: new UntypedFormControl('', []),
    breakStartTime: new UntypedFormControl('', []),
    breakEndTime: new UntypedFormControl('', []),
    startAddress: new UntypedFormControl('', [Validators.required]),
    endAddress: new UntypedFormControl('', [Validators.required]),
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  onFileChange(event:any)
  {
    this.myFiles = [];
      for (var i = 0; i < event.target.files.length; i++)
      { 
          this.myFiles.push(event.target.files[i]);
      }
  }
  submit()
  {
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      console.log(this.form.value);
      this.allFormValue = {
        "fullName":this.form.value.fullName,
        "email":this.form.value.email,
        "mobileNumber":this.form.value.mobileNumber,
        "vehicleType":this.form.value.vehicleType,
        "shiftStartTime":this.form.value.shiftStartTime,
        "shiftEndTime":this.form.value.shiftEndTime,
        "breakStartTime":this.form.value.breakStartTime,
        "breakEndTime":this.form.value.breakEndTime,
        "startAddress":this.startAddress,
        "startLatitude":this.startLatitude,
        "startLongitude":this.startLongitude,
        "endAddress":this.endAddress,
        "endLatitude":this.endLatitude,
        "endLongitude":this.endLongitude,
        "id":this.old_id,
      };
      
      this._http.post(this.editDriverSubmit,this.allFormValue).subscribe((response:any)=>{
        //console.log("response of api"+response);
        this.apiResponse = response;
        if(this.apiResponse.error == false)
        {
          //this.form.reset();
          //this.myFiles = [];
          setTimeout(() => {
            window.history.back();
          }, 2000);
        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}
