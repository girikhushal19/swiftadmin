import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { ActivatedRoute,Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-edit-event-category',
  templateUrl: './edit-event-category.component.html',
  styleUrls: ['./edit-event-category.component.css']
})
export class EditEventCategoryComponent implements OnInit {
  imageSrc: string = '';
  fileInputLabel: string = "";
  myFiles:string [] = [];
  base_url = "";base_url_node = ""; 
  token:any;formData:any;images:any;video:any;user_type:any;apiResponse:any;formValue:any;addDriverSubmit:any;getModel:any;allModelList:any;getCarCategory:any;allCarCategory:any;

  default_date: Date = new Date(); default_date2: Date = new Date();
  default_date3: Date = new Date(); default_date4: Date = new Date();
  //ddDate = new Date('Thu Jan 26 2023 22:38:32 GMT+0530 (India Standard Time)');
  address: string = '';startLatitude: string = '';startLongitude: string = '';
  endAddress: string = '';endLatitude: string = '';endLongitude: string = '';
  allFormValue:any;addrObj:any;edit_id: string = ''; old_category: string = '';old_category2: string = '';old_photos: string = '';
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService, private router: Router)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.baseapiurl2;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      //window.location.href = this.base_url;
    }
    this.addDriverSubmit = this.base_url_node+"api/event/createeditordeletecatagory";
    //this.getModel = this.base_url_node+"getModel";
    //this.getCarCategory = this.base_url_node+"getCarCategory";
    //console.log("here");
    this.edit_id = this.actRoute.snapshot.params['id'];
    this._http.get(this.base_url_node+"api/event/getcatagorybyid/"+this.edit_id).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      if(this.apiResponse.status == true)
      {
        this.allCarCategory = this.apiResponse.data;
        console.log("response of api allCarCategory"+JSON.stringify(this.allCarCategory));
        if(this.allCarCategory.length > 0)
        {
          this.old_category = this.allCarCategory[0].name;
          if(this.allCarCategory[0].photos)
          {
            if(this.allCarCategory[0].photos.length > 0)
            {
              this.old_photos = this.allCarCategory[0].photos[0];
            }
          }
          this.old_category2 = this.allCarCategory[0].name;
        }
        this.apiResponse = {};
      }
    });
  }
  ngOnInit(): void {
      this.myFiles = []; 
      // console.log(this.ddDate);
      // console.log(this.ddDate.getHours());
      // console.log(this.ddDate.getMinutes());
      // console.log(this.ddDate.getSeconds());
  }

 

  form = new UntypedFormGroup({
    category: new UntypedFormControl('', [Validators.required]),
    images: new UntypedFormControl('', []),
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  onFileChange(event:any)
  {
    this.myFiles = [];
      for (var i = 0; i < event.target.files.length; i++)
      { 
          this.myFiles.push(event.target.files[i]);
      }
  }
  submit()
  {
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this.allFormValue = {
        "op":"u",
        "name":this.old_category2,
        "newname":this.form.value.category
      };
      this.formData = new FormData(); 
      this.formData.append('name', this.old_category2 ); 
      this.formData.append('old_photos', this.old_photos ); 
      this.formData.append('newname', this.form.value.category); 
      this.formData.append('op', "u");
      for (var i = 0; i < this.myFiles.length; i++)
      { 
        this.formData.append("image", this.myFiles[i]);
      }

      console.log(this.formData);
      this._http.post(this.addDriverSubmit,this.formData).subscribe((response:any)=>{
        //console.log("response of api"+response);
        this.apiResponse = response;
        if(this.apiResponse.status == true)
        {
          //this.form.reset();
          //this.myFiles = [];
          setTimeout(() => {
            //window.location.reload(); 
            this.router.navigate(['allEventCategory'])
          }, 2000); 
        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}
