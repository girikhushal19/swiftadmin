import { Component, OnInit, ViewChild, ElementRef,AfterViewInit } from '@angular/core';
import { GoogleMap, MapInfoWindow, MapMarker } from '@angular/google-maps';
import {MapDirectionsService} from '@angular/google-maps';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-demo-route-plan',
  templateUrl: './demo-route-plan.component.html',
  styleUrls: ['./demo-route-plan.component.css']
})
 
export class DemoRoutePlanComponent implements OnInit {
  
  markers:any[] = [];
  position:any;
  @ViewChild('map', {static: false}) mapElement:any= ElementRef;
  map:any;
  marker:any;
  
  center = new google.maps.LatLng(22.7611714,75.8619856);
  
  mapOptions: google.maps.MapOptions = {
    center: this.center,
    zoom: 10,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  
  ngAfterViewInit(){

    this.mapInit();
    
    
    var lat_lng = new Array();
    var latlngbounds = new google.maps.LatLngBounds();
    for (let i = 0; i < this.markers.length; i++)
    {
      let m = i+1;
      let icon = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld='+m+'|333333|EA4335';

      var data = this.markers[i];
      var myLatlng = new google.maps.LatLng(data.latitude, data.longitude);
      lat_lng.push(myLatlng);
      var mappppp = this.map;
      var marker = new google.maps.Marker({
        position: myLatlng,
        map: this.map,
        icon:icon,
        title:data.description,
      });
      var infoWindow = new google.maps.InfoWindow({
        content: "contentString",
        ariaLabel: "Uluru",
      });
      // console.log(i)
      //console.log("myLatlng "+myLatlng);
      latlngbounds.extend(myLatlng);
      (function(marker, data) {
        google.maps.event.addListener(marker, "click", function(e:any) {
          infoWindow.setContent(data.timestamp);
          infoWindow.open(mappppp, marker);
        });
      })(marker, data);

      


    }
    //console.log(google.maps.DirectionsTravelMode); 
    //console.log("latlngbounds.getCenter() "+latlngbounds.getCenter());

    this.map.setCenter(latlngbounds.getCenter());
    this.map.fitBounds(latlngbounds);


    var service = new google.maps.DirectionsService();
    var cd = this.map;
    //var DRIVING = google.maps.DirectionsTravelMode.DRIVING;
      //Loop and Draw Path Route between the Points on MAP
      for (var i = 0; i < lat_lng.length; i++)
      {
        if ((i + 1) < lat_lng.length)
        {
          var src = lat_lng[i];
          var des = lat_lng[i + 1];
          // path.push(src);
          //console.log(des);
          service.route({
            origin: src,
            destination: des,
            travelMode: google.maps.TravelMode.DRIVING,
          }, function(result:any, status) {
            if (status == google.maps.DirectionsStatus.OK) {

              //Initialize the Path Array
              // var path = new google.maps.MVCArray();
              // //Set the Path Stroke Color
              // var poly = new google.maps.Polyline({
              //   map: cd,
              //   strokeColor: "#4A89F3",
              //   strokeOpacity: 1.0,
              //   strokeWeight: 8,
              // });
              // poly.setPath(path);
              // for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
              //   path.push(result.routes[0].overview_path[i]);
              // }
            }
          });
        }
      }
  }  
  
  mapInit(){
    this.map = new google.maps.Map(this.mapElement.nativeElement,
    this.mapOptions);
  }

  

  constructor() {
    this.map= google.maps.Map;
    
    marker: google.maps.Marker;

    var html = "";
    html += '<div class="stop-info-dialogue ui-widget-content ui-draggable ui-draggable-handle" id="modal-stop-detail" style="display: block;">';
    html += '<div class="stop-info-header d-flex justify-content-between align-items-cemnter" style="background-color: rgb(0, 0, 0);">';
      html += '<h6 class="stop_info_header">Stop No - 2 (Sadik)</h6>';
      html += '<ul class="d-flex align-items-center">';
         html += '<li><a href="javascript:editStopReviewRoute(1798995);" title="Edit" class="editStop"><img src="https://cdncrew.upperinc.com/public/assets/img/stop-edit.svg" class="img-fluid" alt="Edit"></a></li>';
         html += '<li><a href="javascript:beforeDelete(1798995);" title="Delete" class="deleteStop"><img src="https://cdncrew.upperinc.com/public/assets/img/delete-icon.svg" class="img-fluid" alt="Delete"></a></li>';
         html += '<li><a href="#" title="Close" class="close-icon"><img src="https://cdncrew.upperinc.com/public/assets/img/stop-close.svg" class="img-fluid" alt="Close"></a></li>';
      html += '</ul>';
   html += '</div>';
   html += '<div class="stop-info-body">';
      html += '<ul class="d-flex flex-wrap">';
         html += '<li>';
            html += '<label>Address</label>';
            html += '<p class="" id="view_stop_address" data-toggle="tooltip" data-placement="bottom" title="New Delhi, Delhi, India" data-original-title="">New Delhi, Delhi, India</p>';
         html += '</li>';
         html += '<li>';
            html += '<label>ETA</label>';
            html += '<p class="" id="view_stop_arrived_time">07:50 AM</p>';
         html += '</li>';
         html += '<li>';
            html += '<label>Nick Name</label>';
            html += '<p class="" id="view_stop_v_nick_name"></p>';
         html += '</li>';
         html += '<li>';
            html += '<label>Contact Name</label>';
            html += '<p class="" id="view_stop_contact_name">-</p>';
         html += '</li>';
         html += '<li>';
            html += '<label>Company Name</label>';
            html += '<p class="" id="view_stop_company_name">-</p>';
         html += '</li>';
         html += '<li>';
            html += '<label>Phone</label>';
            html += '<p class="" id="view_stop_phone">-</p>';
         html += '</li>';
         html += '<li>';
            html += '<label>Email</label>';
            html += '<p class="" id="view_stop_email">-</p>';
         html += '</li>';
         html += '<li>';
            html += '<label>Stop Type</label>';
            html += '<p class="" id="view_stop_type">None</p>';
         html += '</li>';
         html += '<li>';
            html += '<label>Priority</label>';
            html += '<p class="" id="view_stop_priority">Medium</p>';
         html += '</li>';
         html += '<li>';
            html += '<label>Notes</label>';
            html += '<p class="" id="view_stop_notes" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="">-</p>';
         html += '</li>';
         html += '<li>';
            html += '<label>Time window</label>';
            html += '<p class="" id="view_stop_time_window">-</p>';
         html += '</li>';
      html += '</ul>';
    html += '</div>';
    html += '</div>';

  this.markers = 
  [
    {
      "id":1,
      "timestamp": 'Vijay Nagar, Indore, Madhya Pradesh 452010, India',
      "latitude":"22.7529391",
      "longitude":"75.8915147",
      "description": '',
      "html":"html 1",
      "htmlView":html
    },
    {
      "id":2,
      "timestamp": 'Khajrana Ganesh Mandir, Ganeshpuri Main Road, Ganeshpuri, Khajrana, Indore, Madhya Pradesh, India',
      "latitude":"22.731233",
      "longitude":"75.9059602",
      "description": '',
      "html":"html 2",
      "htmlView":html
    },
    {
      "id":3,
      "timestamp": 'New Father Angel H.S. School, Dubey Colony, Bajrang Nagar, Indore, Madhya Pradesh, India',
      "latitude":"22.7476036",
      "longitude":"75.8783642",
      "description": '',
      "html":"html 3",
      "htmlView":html
    },
    {
      "id":4,
      "timestamp": 'C21 Mall, AB Road, Scheme 54 PU4, Indore, Madhya Pradesh',
      "latitude":"22.7441207",
      "longitude":"75.8918344",
      "description": '',
      "html":"html 4",
      "htmlView":html
    },
    {
      "id":5,
      "timestamp": 'Saboro Mahindra Milk, LIG Colony, Indore, Madhya Pradesh',
      "latitude":"22.741983",
      "longitude":"75.8842573",
      "description": '',
      "html":"html 5",
      "htmlView":html
    },
    {
      "id":6,
      "timestamp": 'Malwa Mill, Indore, Madhya Pradesh',
      "latitude":"22.7369346",
      "longitude":"75.8716145",
      "description": '',
      "html":"html 6",
      "htmlView":html
    },
    {
      "id":7,
      "timestamp": 'Durgesh Photo Framing, New Dewas Road, Patni Pura, Indore, Madhya Pradesh, India',
      "latitude":"22.738234",
      "longitude":"75.8772701",
      "description": '',
      "html":"html 7",
      "htmlView":html
    }
  ];

        

  }
  ngOnInit() {
    //console.log(this.markers);
    //console.log(this.markers[0].latitude);
    
  }
  
  

}

