import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { Editor } from 'ngx-editor';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';

@Component({
  selector: 'app-markerplace',
  templateUrl: './markerplace.component.html',
  styleUrls: ['./markerplace.component.css']
})
export class MarkerplaceComponent implements OnInit {
  dropdownList : any[] = [];
  dropdownListNew : any[] = [];
  selectedItems : any[] = [];
  zone_list:any;
     dropdownSettings = {};
     records:any;
     imageSrc: string = '';
  fileInputLabel: string = "";
  base_url = "";base_url_node = ""; base_url_two:string='';
  token:any;formData:any;images:any;video:any;user_type:any;apiResponse:any;formValue:any;sendMerchantPushNotificationSubmit:any;getModel:any;allModelList:any;getCarCategory:any;allCarCategory:any;getAllActiveUserApi:any;queryParam:any;record:any;

  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  {
    
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }


    this.getAllActiveUserApi = this.base_url_node+"getAllActiveMerchantAdminApi";
    this.base_url_two = this.loginAuthObj.baseapiurl2;

    this._http.get(this.base_url_two+"api/shipping/geAlltZones").subscribe(res=>{
      console.log(res)
      this.apiResponse = res;
      if(this.apiResponse.status == true)
      {
        this.record = this.apiResponse.data; 
        console.log("hereeeeeeeeeeeeeeeeeeeeeeeee");
        console.log(this.record);
        var abc = [];
        for (var _i = 0; _i < this.record.length; _i++)
        {
          abc.push({item_id: this.record[_i].name,item_text: this.record[_i].name});
          this.dropdownListNew = abc;
          //console.log(this.dropdownListNew);
          //this.dropdownListNew[_i] = { item_id: this.record[_i]._id, item_text: this.record[_i].firstName+" "+this.record[_i].lastName };
        }
      }
      this.apiResponse = {};
    })



     
  }

  ngOnInit(): void {

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Sélectionner tout',
      unSelectAllText: 'Ne pas sélectionner tout',
      itemsShowLimit: 10,
      allowSeachFilter: true
   } 

  }

  onItemSelect(item: any) {
    //console.log(item);
  }
  onSelectAll(items: any) {
    //console.log(items);
  }


  form = new UntypedFormGroup({
    method_name: new UntypedFormControl('', [Validators.required]),
    zone_name: new UntypedFormControl('', [Validators.required]),
    min_weight: new UntypedFormControl('', [Validators.required]),
    max_weight: new UntypedFormControl('', [Validators.required]),
    cost_base: new UntypedFormControl('', [Validators.required]),
    cost_additional_weight: new UntypedFormControl('', [Validators.required]),
    cose_per_unit: new UntypedFormControl('', [Validators.required]),
    number_of_days: new UntypedFormControl('', [Validators.required]),
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  

  submit()
  {
     
    //console.log(this.form.value);   
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      var abc: any[] = []
      this.formValue = this.form.value;
      //console.log(this.formValue);  
      this.form.value.zone_name.forEach(function(value:any){
        //console.log(value);
        //zone_list.push({"zone":value.item_id});
        //this.zone_list.push({"01": value.item_id, "02": value.item_id});
        //this.zone_list = {"01": value.item_id, "02": value.item_id};
        abc.push({"zone":value.item_id});
        
      });
      console.log(abc);

      

      let queryParam = {  
        "zone_list":abc,
        "type":"m",
        "method_name":this.form.value.method_name,
        "min_weight":this.form.value.min_weight,
        "max_weight":this.form.value.max_weight,
        "cost_base":this.form.value.cost_base,
        "cost_additional_weight":this.form.value.cost_additional_weight,
        "cose_per_unit":this.form.value.cose_per_unit,
        "number_of_days":this.form.value.number_of_days,
      };
      this._http.post(this.base_url_two + "api/shipping/addShippinMethodmarketplace", queryParam).subscribe(res => {
        console.log(res);
        this.apiResponse = res;
        if(this.apiResponse.status == true)
        {
          window.location.reload()
        }
        //
      })

    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }


}
