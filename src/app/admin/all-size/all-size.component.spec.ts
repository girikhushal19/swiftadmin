import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllSizeComponent } from './all-size.component';

describe('AllSizeComponent', () => {
  let component: AllSizeComponent;
  let fixture: ComponentFixture<AllSizeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllSizeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllSizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
