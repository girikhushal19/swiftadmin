import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginauthenticationService } from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators, FormGroup, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.component.html',
  styleUrls: ['./editprofile.component.css']
})
export class EditprofileComponent implements OnInit {
  base_url: any;base_url_two:string="";
  usersarray: any;
  myFiles:string [] = [];formData:any;
  users: any;old_userImage:string="";apiResponse:any;
  // form !: FormGroup
  form = new FormGroup({
    images: new FormControl('', []),
    email: new FormControl('', [Validators.required]),
    first_name: new FormControl('', [Validators.required]),
    last_name: new FormControl('', [Validators.required]),
    phone: new FormControl('', [Validators.required]),
    is_active: new FormControl('', [Validators.required]),
    pw: new FormControl('', []),
    from: new FormControl('admin', []),
  });
  constructor(private http: HttpClient, private activeRoute: ActivatedRoute, private loginAuthObj: LoginauthenticationService,
    private router:Router) {
    this.base_url = loginAuthObj.baseapiurl;
    this.base_url_two = loginAuthObj.baseapiurl2;
    this.users = []
    this.usersarray = []


  }

  ngOnInit(): void {
    //console.log(this.activeRoute.snapshot.params['id'])
    let user_id = this.activeRoute.snapshot.params['id'];
    this.http.get(this.base_url + "getSingleUsers/"+user_id).subscribe(res => {
      //console.log("hereee 40");
      this.usersarray = res
      //this.users = this.usersarray.data;
      if(this.usersarray.status == true)
      {
        const user = this.usersarray.data[0];
        //console.log("usersarray "+JSON.stringify(this.usersarray));
        // const user = this.users.find((a: any) => {
        //   return a._id === this.activeRoute.snapshot.params['id']
        // })

        if (user) {
          //console.log("dsfdfdsf"+JSON.stringify(user.photo));
          if(user.photo)
          {
            this.old_userImage = user.photo;
          }
          this.form = new UntypedFormGroup({
            images: new FormControl('', []),
            email: new FormControl(user.email, [Validators.required]),
            first_name: new FormControl(user.first_name, [Validators.required]),
            last_name: new FormControl(user.last_name, [Validators.required]),
            is_active: new FormControl(user.is_active, [Validators.required]),
            phone: new FormControl(user.phone, [Validators.required]),
            pw: new FormControl('', []),
            from: new FormControl('admin', []),


          });
          
        }

      }
      
      
      
    })

    this.myFiles = [];

  }

  onFileChange(event:any)
  {
    this.myFiles = [];
      for (var i = 0; i < event.target.files.length; i++)
      { 
          this.myFiles.push(event.target.files[i]);
      }
  }

  submit() {
    console.log(this.form.value)

    this.formData = new FormData(); 
        this.formData.append('first_name', this.form.value.first_name);
        this.formData.append('last_name', this.form.value.last_name);
        this.formData.append('email', this.form.value.email);
        this.formData.append('is_active', this.form.value.is_active);
        this.formData.append('phone', this.form.value.phone);
         
        this.formData.append('newpassword', this.form.value.pw);
        this.formData.append('from','admin');
        for (var i = 0; i < this.myFiles.length; i++)
        { 
          this.formData.append("photo", this.myFiles[i]);
        } 
    this.http.post(this.base_url + "users", this.formData).subscribe(res=>{
      console.log("response  ---->>>>>   "+res);
      //this.router.navigate(['users'])
      this.apiResponse = res;
      if(this.apiResponse.status == true)
      {
        setTimeout(() => {
          ///this.router.navigate(['users'])
        }, 2000); 
      }

    })
  }

}
