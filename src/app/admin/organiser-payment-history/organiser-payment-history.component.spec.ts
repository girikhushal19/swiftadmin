import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganiserPaymentHistoryComponent } from './organiser-payment-history.component';

describe('OrganiserPaymentHistoryComponent', () => {
  let component: OrganiserPaymentHistoryComponent;
  let fixture: ComponentFixture<OrganiserPaymentHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrganiserPaymentHistoryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrganiserPaymentHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
