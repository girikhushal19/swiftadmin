import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-add-event-banner',
  templateUrl: './add-event-banner.component.html',
  styleUrls: ['./add-event-banner.component.css']
})
export class AddEventBannerComponent implements OnInit {
  imageSrc: string = '';
  fileInputLabel: string = "";
  myFiles:string [] = [];
  base_url = "";base_url_node = ""; 
  token:any;formData:any;images:any;video:any;user_type:any;apiResponse:any;formValue:any;addDriverSubmit:any;getModel:any;allModelList:any;getCarCategory:any;allCarCategory:any;

  default_date: Date = new Date(); default_date2: Date = new Date();
  default_date3: Date = new Date(); default_date4: Date = new Date();
  //ddDate = new Date('Thu Jan 26 2023 22:38:32 GMT+0530 (India Standard Time)');
  address: string = '';startLatitude: string = '';startLongitude: string = '';
  endAddress: string = '';endLatitude: string = '';endLongitude: string = '';
  allFormValue:any;addrObj:any;
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.baseapiurl2;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      //window.location.href = this.base_url;
    }
    this.addDriverSubmit = this.base_url_node+"api/event/createeditbanner";
    //this.getModel = this.base_url_node+"getModel";
    //this.getCarCategory = this.base_url_node+"getCarCategory";
    //console.log("here");
    
  }
  ngOnInit(): void {
      this.myFiles = []; 
      // console.log(this.ddDate);
      // console.log(this.ddDate.getHours());
      // console.log(this.ddDate.getMinutes());
      // console.log(this.ddDate.getSeconds());
  }

 

  form = new UntypedFormGroup({
    category: new UntypedFormControl('', [Validators.required]),
    second_text: new UntypedFormControl('', []),
    images: new UntypedFormControl('', [Validators.required]),
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  onFileChange(event:any)
  {
    this.myFiles = [];
      for (var i = 0; i < event.target.files.length; i++)
      { 
          this.myFiles.push(event.target.files[i]);
      }
  }

  submit()
  {
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this.allFormValue = {
        "op":"c",
        "name":this.form.value.category
      };
      this.formData = new FormData(); 
      this.formData.append('name', this.form.value.category); 
      this.formData.append('second_text', this.form.value.second_text); 
      this.formData.append('op', "c");
      for (var i = 0; i < this.myFiles.length; i++)
      { 
        this.formData.append("image", this.myFiles[i]);
      }

      console.log(this.allFormValue);
      this._http.post(this.addDriverSubmit,this.formData).subscribe((response:any)=>{
        //console.log("response of api"+response);
        this.apiResponse = response;
        if(this.apiResponse.status == true)
        {
          //this.form.reset();
          //this.myFiles = [];
          setTimeout(() => {
            window.location.reload();
          }, 2000); 
        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}
