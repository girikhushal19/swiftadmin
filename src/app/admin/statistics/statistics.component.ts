import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { Chart, registerables } from 'chart.js';
Chart.register(...registerables)
@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {
  homedata: any;
  labeldays: any;
  totalsales: any;
  totalorders: any;
  totalcommissions: any
  orderpie: any
  base_url:any;
  
  constructor(private http: HttpClient,private loginAuthObj:LoginauthenticationService) {
    this.homedata = []
    this.labeldays = []
    this.totalsales = []
    this.totalorders = []
    this.totalcommissions = []
    this.orderpie = []
    this.base_url = loginAuthObj.baseapiurl
  }

  ngOnInit(): void {
    this.Renderchart()

  }
  Renderchart() {

    this.http.get(this.base_url).subscribe(res => {
      this.homedata = res
      this.labeldays = this.homedata.data.numberofdaysarray
      this.totalsales = this.homedata.data.totalsalesarray
      this.totalcommissions = this.homedata.data.totalcommissionarray
      this.totalorders = this.homedata.data.totalordersarray
      this.orderpie = this.homedata.data.orderspie

      new Chart('linechart', {
        type: 'line',
        data: {
          labels: this.labeldays,
          datasets: [{
            label: 'Total Sales',
            data: this.totalsales,
            borderWidth: 1,
            fill: false,
            borderColor: 'rgb(0, 143, 251)',
            tension: 0.1
          },
          {
            label: 'Total Orders',
            data: this.totalorders,
            borderWidth: 1,
            fill: false,
            borderColor: 'rgb(254, 176, 25)',
            tension: 0.1
          },
          {
            label: 'Total Commissions',
            data: this.totalcommissions,
            borderWidth: 1,
            fill: false,
            borderColor: 'rgb(119, 93, 208)',
            tension: 0.1
          }
          ]
        },
        options: {
          scales: {
            y: {
              beginAtZero: true,
              grid: {
                display: false,
              },
            },
            x: {
              grid: {
                display: false,
              },
            }
          }
        }
      });

      new Chart('piechart', {
        type: 'doughnut',
        data: {
          labels: [
            'Total',
            'Completed',
            'Processing',
            'Cancelled',
            'Refunded',
          ],
          datasets: [{
            label: 'My First Dataset',
            data: this.orderpie,

            backgroundColor: [
              '#2171ae',
              '#28a745',
              '#ffc107',
              'rgb(255, 69, 96)',
              '#17a2b8'
            ],
            hoverOffset: 4
          }]
        },
        options: {
         
          plugins: {
            legend: {
              position: 'left'
            }

          }

        }
      });

      new Chart('polar', {
        type: 'doughnut',
        data: {
          labels: [
            'Total',
            'Timmne',
            'Expedie',
            'En course de livration',

          ],
          datasets: [{
            label: 'My First Dataset',
            data: [10, 5, 3, 2],
            backgroundColor: [
              'rgb(0, 143, 251)',
              'rgb(254, 176, 25)',
              'rgb(255, 69, 96)',
              'rgb(119, 93, 208)',
              'rgb(0, 216, 182)'
            ],
            hoverOffset: 4
          }]
        },
        options: {
          
          plugins: {
            legend: {
              position: 'left'
            },
            
          }

        }
      });
    })




  }

}
