import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, AbstractControl, FormControl, Validators } from '@angular/forms';
import { LoginauthenticationService } from '../../adminservice/loginauthentication.service';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AdmincategoryComponent } from '../admincategory/admincategory.component';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Options } from 'ngx-google-places-autocomplete/objects/options/options';

@Component({
  selector: 'app-modifyproduct',
  templateUrl: './modifyproduct.component.html',
  styleUrls: [
    './modifyproduct.component.css',
    'assets/build/css/style.css',
    'assets/build/css/seller.css',
    "assets/build/css/materialdesignicons.min.css",
    "assets/build/css/vendor.bundle.base.css",


  ]
})
export class ModifyproductComponent implements OnInit {
  userForm!: FormGroup;
  phone: any;
  public products = [
    {
      id: 1,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 2,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 3,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 4,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 5,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 6,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 7,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 8,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 9,
      imgUrl: "",
      imgBase64Data: ""
    }
  ];
  url: any
  format: any
  base_url: any
  token: any
  user_type: any
  productarray: any
  product: any
  form!: FormGroup


  constructor(
    private fb: FormBuilder, public dialog: MatDialog, private loginAuthObj: LoginauthenticationService,
    private http: HttpClient, private activeRoute: ActivatedRoute, private router: Router
  ) {

    this.base_url = this.loginAuthObj.baseapiurl2
    this.productarray = []
    this.product = []


    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    console.log(this.token);
    console.log(this.user_type);
    if (this.token === "" || this.user_type !== "admin") {
      // window.location.href = "http://localhost:4200/";
    }
    this.userForm = this.fb.group({
      name: [],
      phones: this.fb.array([
        this.fb.control(null)
      ]),
      phones2: this.fb.array([
        this.fb.control(null)
      ]),
      sku: new FormControl('', [Validators.required]),
      title: new FormControl('', [Validators.required]),
    })
  }

  ngOnInit(): void {
    this.http.get(this.base_url+"api/product/getproductbyidtoedit/63bd02e6dd72d79dfcd84c83").subscribe(res => {
      this.productarray = res
      this.product = this.productarray.data

      console.log(this.product)
      this.userForm = this.fb.group({
        name: [],
        phones: this.fb.array([
          this.fb.control(null)
        ]),
        phones2: this.fb.array([
          this.fb.control(null)
        ]),
        sku: new FormControl(this.product.sku, [Validators.required]),
        title: new FormControl(this.product.title, [Validators.required]),
      })
    })

  }
  onFileUpdate(event: any, index: any) {
    console.log(index)
    const files = event.target.files;
    if (files.length === 0) return;

    const reader = new FileReader();

    reader.readAsDataURL(files[0]);
    reader.onload = _event => {

      this.products[index].imgBase64Data = reader.result as string;
    };
  }


  onSelectFile(event: any) {
    const file = event.target.files && event.target.files[0];
    if (file) {
      var reader = new FileReader();
      reader.readAsDataURL(file);
      if (file.type.indexOf('image') > -1) {
        this.format = 'image';
      } else if (file.type.indexOf('video') > -1) {
        this.format = 'video';
      }
      reader.onload = (event) => {
        this.url = (<FileReader>event.target).result;
      }
    }
  }

  addPhone(): void {
    (this.userForm.get('phones') as FormArray).push(
      this.fb.control(null)
    );
  }

  addPhone2(): void {
    (this.userForm.get('phones2') as FormArray).push(
      this.fb.control(null)
    );
  }

  removePhone(index: any) {
    (this.userForm.get('phones') as FormArray).removeAt(index);
  }

  removePhone2(index: any) {
    (this.userForm.get('phones2') as FormArray).removeAt(index);
  }


  getPhonesFormControls(): AbstractControl[] {
    return (<FormArray>this.userForm.get('phones')).controls
  }

  getPhonesFormControls2(): AbstractControl[] {
    return (<FormArray>this.userForm.get('phones2')).controls
  }

  // form = new FormGroup({
  //   sku: new FormControl('ASD#2515', [Validators.required]),
  //   title: new FormControl('god knows what is is rajes', [Validators.required]),
  //   description: new FormControl('ok what is its', [Validators.required]),
  //   price: new FormControl(2000, [Validators.required]),
  //   id: new FormControl(this.activeRoute.snapshot.params['id'], [Validators.required]),
  //   catagory: new FormControl('Access', [Validators.required]),
  //   photo: new FormControl('', [Validators.required]),
  //   dangerous_goods: new FormControl('false', [Validators.required]),
  //   video: new FormControl('siri_coffee_sculpture-wallpaper-1920x1080.jpg', [Validators.required]),
  //   attributes: new FormControl([
  //     {
  //       "variation_name": "isramthere",
  //       "variation_options": [
  //         {
  //           "option": "yes",
  //           "option_value": "yes"
  //         }

  //       ]
  //     }, {
  //       "variation_name": "ramsize",
  //       "variation_options": [
  //         {
  //           "option": "8gb",
  //           "option_value": "8gb"
  //         }

  //       ]
  //     }
  //   ], [Validators.required]),
  //   sales_info: new FormControl([
  //     {
  //       "variation_name": "color",
  //       "variation_options": [
  //         {
  //           "option": "Purple",
  //           "option_value": "Purple"
  //         },
  //         {
  //           "option": "Megenta",
  //           "option_value": "Megenta"
  //         },
  //         {
  //           "option": "Red",
  //           "option_value": "Green"
  //         },
  //         {
  //           "option": "Red",
  //           "option_value": "Blue"
  //         },
  //         {
  //           "option": "Red",
  //           "option_value": "Black"
  //         }
  //       ]
  //     }
  //     ,
  //     {
  //       "variation_name": "Brand",
  //       "variation_options": [
  //         {
  //           "option": "Syn"
  //         },
  //         {
  //           "option": "Gold"
  //         },
  //         {
  //           "option": "Samsung"
  //         },
  //         {
  //           "option": "SpaceX"
  //         },
  //         {
  //           "option": "Clovefield"
  //         }
  //       ]
  //     }
  //   ], [Validators.required]),
  //   dispatch_info: new FormControl({
  //     "weight": 200, "size": { "width": 10, "Length": 20, "Height": 30 },
  //     "shipping_details": {
  //       "some": "one"
  //     }
  //   }, [Validators.required]),
  //   other_details: new FormControl({
  //     "preorder": true,
  //     "shippingtime": "in days",
  //     "condition": "new",
  //     "parent_sku": "somesome"
  //   }, [Validators.required]),
  //   provider_id: new FormControl('63bd02365f57d397516e8f2f', [Validators.required]),
  //   weight: new FormControl(300, [Validators.required]),
  //   size: new FormControl({
  //     "width": 200,
  //     "Length": 300,
  //     "Height": 400
  //   }, [Validators.required]),

  // });




  send(values: any) {
    this.form = new FormGroup({
      sku: new FormControl(this.userForm.value.sku, [Validators.required]),
      title: new FormControl(this.userForm.value.title, [Validators.required]),
      id: new FormControl(this.activeRoute.snapshot.params['id'], [Validators.required]),
    })
    console.log(this.activeRoute.snapshot.params['id'])
    console.log(this.userForm.value)
    this.http.post(this.base_url+"api/product/createproduct", this.form.value).subscribe(res => {
      alert("Product updated succesfully")
      this.router.navigate(['all_products'])

      console.log(res)
    })

  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AdmincategoryComponent, {
      height: '400px',
      width: '700px'
    });
  }




}
