import { Component, OnInit } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { EditprofileComponent } from '../editprofile/editprofile.component';
import { HttpClient } from '@angular/common/http';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { Options } from 'ngx-google-places-autocomplete/objects/options/options';
@Component({
  selector: 'app-statistics-o',
  templateUrl: './statistics-o.component.html',
  styleUrls: ['./statistics-o.component.css']
})
export class StatisticsOComponent implements OnInit {
  searchText:any;
  select_all = false;
  usersarray:any;
  users:any
  orders:any;
  base_url:any;
  constructor(public dialog: MatDialog,private http: HttpClient,private loginAuthObj:LoginauthenticationService)
  {
    this.users =[]
    this.usersarray = []
    this.orders = []
    this.base_url = loginAuthObj.baseapiurl2

    this.http.get(this.base_url+"api/subs/getAllSubscriptions").subscribe(res => {
      this.orders = res;
      console.log("this.orders ->>>> "+this.orders);
    })
  }

  ngOnInit(): void {
    // this.http.get(this.base_url+"api/subs/getAllSubscriptions").subscribe(res => {
    //   this.orders = res;
    // })
  }

  data: any[] = []


}
