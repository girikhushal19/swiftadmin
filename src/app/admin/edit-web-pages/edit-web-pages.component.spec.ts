import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditWebPagesComponent } from './edit-web-pages.component';

describe('EditWebPagesComponent', () => {
  let component: EditWebPagesComponent;
  let fixture: ComponentFixture<EditWebPagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditWebPagesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditWebPagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
