import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { FormGroup, FormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-all-push-notification',
  templateUrl: './all-push-notification.component.html',
  styleUrls: ['./all-push-notification.component.css']
})
export class AllPushNotificationComponent implements OnInit {

  base_url = "";base_url_node = "";
  addModelsSubmit:any;getModel:any;allModelList:any;
  token:any;user_type:any;apiResponse:any;formValue:any;record:any;totalPageNumber:any;allAdminPushNotification:any;queryParam:any;numbers:any;allAdminPushNotificationCount:any;apiStringify:any;deleteBanner:any;base_url_node_plain:any;myJson:any;
  selectedIndex: number;deletePushNotificationApi:any;user_id:any;
  unchecked:boolean = false;
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService) { 

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_plain = this.loginAuthObj.base_url_node;

    this.user_id = this.loginAuthObj.userLoggedIn();
    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }

    this.allAdminPushNotificationCount = this.base_url_node+"allAdminPushNotificationCount";
    //console.log(this.allAdminPushNotificationCount);
    this.allAdminPushNotification = this.base_url_node+"allAdminPushNotification";
    this.deletePushNotificationApi = this.base_url_node+"deletePushNotificationApi";
    this.getallBanner(0);
    this.selectedIndex = 0;
  }

  ngOnInit(): void {
    this.queryParam = {"user_id":this.user_id};
    this._http.post(this.allAdminPushNotificationCount,this.queryParam).subscribe((response:any)=>{
      //console.log("allAdminPushNotificationCount"+response);
      this.totalPageNumber = response.totalPageNumber;
      this.numbers = Array(this.totalPageNumber).fill(0).map((m,n)=>n);
      //console.log("this.numbers "+this.numbers);
    });
  }
  getallBanner(numofpage=0){
    this.selectedIndex = numofpage;
    //console.log("numofpage"+numofpage);

    this.queryParam = {"numofpage":numofpage,"user_id":this.user_id};
    this._http.post(this.allAdminPushNotification,this.queryParam).subscribe((response:any)=>{
      //console.log("allAdminPushNotification"+response);
      this.apiStringify = JSON.stringify(response);
      //console.log("result"+this.apiStringify);
      //console.log(this.apiStringify.error);
      this.record = response.record;
      console.log("this record "+this.record);
    });
  }
  deletePushNotification(id:string='')
  {
    this.queryParam = {"id":id};
    this._http.post(this.deletePushNotificationApi,this.queryParam).subscribe((response:any)=>{
      window.location.reload();
      this.getallBanner(0)
    });
    console.log("id "+id);
  }
  select_all = false;
  onSelectAll(e: any): void { 
   console.log("is checked "+e);
    for (let i = 0; i < this.record.length; i++)
    {
      const item = this.record[i];
      item.is_checked = e;
      console.log(item._id);
    }
  }
  onSelectAll2(id:any,event:any)
  {
    console.log("id"+id);
    console.log("event"+event);
  }
 
}