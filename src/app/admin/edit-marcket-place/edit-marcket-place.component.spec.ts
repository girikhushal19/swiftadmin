import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMarcketPlaceComponent } from './edit-marcket-place.component';

describe('EditMarcketPlaceComponent', () => {
  let component: EditMarcketPlaceComponent;
  let fixture: ComponentFixture<EditMarcketPlaceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditMarcketPlaceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditMarcketPlaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
