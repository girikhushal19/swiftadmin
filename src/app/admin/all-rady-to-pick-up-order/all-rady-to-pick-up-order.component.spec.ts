import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllRadyToPickUpOrderComponent } from './all-rady-to-pick-up-order.component';

describe('AllRadyToPickUpOrderComponent', () => {
  let component: AllRadyToPickUpOrderComponent;
  let fixture: ComponentFixture<AllRadyToPickUpOrderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllRadyToPickUpOrderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllRadyToPickUpOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
