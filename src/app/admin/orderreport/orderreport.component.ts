import { Component, OnInit } from '@angular/core';
import { Chart, registerables } from 'chart.js';
Chart.register(...registerables);
import { HttpClient } from '@angular/common/http';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
@Component({
  selector: 'app-orderreport',
  templateUrl: './orderreport.component.html',
  styleUrls: ['./orderreport.component.css']
})
export class OrderreportComponent implements OnInit {
  myLineChart:any;
  base_url:any;apiResponse:any;
  record:any;numberofdaysarray:any;totalordersarray:any;
  totalearnings :number=0;totalcommissionearned:number=0;
  salesthismonth :number=0;sellerthismonthnotapproved:number=0;
  totalwithdrawls :number=0; totalproductsthismonth:number=0;
  constructor(private http: HttpClient,private loginAuthObj:LoginauthenticationService)
  {
    this.base_url = loginAuthObj.baseapiurl;
    this.http.post(this.base_url+"reports",{}).subscribe(res => {
      this.apiResponse = res;
      if(this.apiResponse.status == true)
      {
        this.totalearnings = this.apiResponse.data.totalearnings;
        this.totalcommissionearned = this.apiResponse.data.totalcommissionearned;
        this.salesthismonth = this.apiResponse.data.salesthismonth;
        this.sellerthismonthnotapproved = this.apiResponse.data.sellerthismonthnotapproved;
        this.totalwithdrawls = this.apiResponse.data.totalwithdrawls;
        this.totalproductsthismonth = this.apiResponse.data.totalproductsthismonth;

        this.numberofdaysarray = this.apiResponse.data.numberofdaysarray;
        this.totalordersarray = this.apiResponse.data.totalordersarray;
        this.Renderchart();
      }
      
    });
    
  }

  ngOnInit(): void {
   
  }

  Renderchart(from_date:string='',to_date:string='') {
    
   

    const ctx = document.getElementById('myChart');
    this.myLineChart = new Chart('linechart', {
      type: 'line',
      data: {
        labels: this.numberofdaysarray,
        datasets: [{
          label: 'Ventes totales',
          data: this.totalordersarray,
          borderWidth: 1,
          fill: false,
          borderColor: '#2171ae',
          tension: 0.1
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });
    

  }

  form = new UntypedFormGroup({
    form_date: new UntypedFormControl('', [Validators.required]),
    to_date: new UntypedFormControl('', [Validators.required]),
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  submit()
  {
    if(this.form.valid)
    {
      console.log(this.form.value);
      //this.Renderchart(this.form.value.form_date,this.form.value.to_date);
      let queryParam = {"start_date":this.form.value.form_date,"end_date":this.form.value.to_date};
      this.http.post(this.base_url+"reports",queryParam).subscribe(res => {
        this.apiResponse = res;
        if(this.apiResponse.status == true)
        {
          this.totalearnings = this.apiResponse.data.totalearnings;
          this.totalcommissionearned = this.apiResponse.data.totalcommissionearned;
          this.salesthismonth = this.apiResponse.data.salesthismonth;
          this.sellerthismonthnotapproved = this.apiResponse.data.sellerthismonthnotapproved;
          
          this.numberofdaysarray = this.apiResponse.data.numberofdaysarray;
          this.totalordersarray = this.apiResponse.data.totalordersarray;
          this.myLineChart.destroy();
          this.Renderchart();
        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }

}
