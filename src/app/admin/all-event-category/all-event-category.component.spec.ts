import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllEventCategoryComponent } from './all-event-category.component';

describe('AllEventCategoryComponent', () => {
  let component: AllEventCategoryComponent;
  let fixture: ComponentFixture<AllEventCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllEventCategoryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllEventCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
