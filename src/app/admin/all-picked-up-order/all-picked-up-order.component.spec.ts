import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllPickedUpOrderComponent } from './all-picked-up-order.component';

describe('AllPickedUpOrderComponent', () => {
  let component: AllPickedUpOrderComponent;
  let fixture: ComponentFixture<AllPickedUpOrderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllPickedUpOrderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllPickedUpOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
