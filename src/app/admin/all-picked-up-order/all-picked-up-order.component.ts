import { Component, OnInit,HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginauthenticationService } from '../../adminservice/loginauthentication.service';
import { Options } from 'ngx-google-places-autocomplete/objects/options/options';
import { PrintService } from '../print.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';


@Component({
  selector: 'app-all-picked-up-order',
  templateUrl: './all-picked-up-order.component.html',
  styleUrls: ['./all-picked-up-order.component.css']
})
export class AllPickedUpOrderComponent implements OnInit {
  select_all = false;
  searchText:any;
  page_no:number=0;numbers:any;selectedIndex: number=0;
  base_url: any;base_url_two: any;
  ordersarray:any;users:any;
  orders:any;apiResponse:any;

  api_response_var:number=0;
  totalPageNumber:number=1;pageStart:number=0;totalAdcount:number=0;pageNo:number=1;
  apiresponse:any
  constructor(private http: HttpClient, private loginAuthObj: LoginauthenticationService,
    public printService: PrintService) {
    this.base_url = loginAuthObj.baseapiurl
    this.base_url_two =  loginAuthObj.baseapiurl2;
    this.ordersarray =[]
    this.orders =[]

    this.http.get(this.base_url_two + "api/user/getalluserforfilter", {}).subscribe(res=>{
      this.apiResponse = res
      this.users = this.apiResponse.data
      //console.log("users =====> "+this.users)
    })

    let queryParam1 = {pageno:null};
    this.http.post(this.base_url + "getAllPickedUpByAdminCount", queryParam1).subscribe(res=>{
      this.ordersarray = res
      //this.orders = this.ordersarray.data
      //console.log("page no . --> "+JSON.stringify(res));
      if(this.ordersarray.status == true)
      {
        this.page_no = this.ordersarray.pages;
        this.numbers = Array(this.page_no).fill(0).map((m,n)=>n);
        //console.log("this.numbers ---->  "+this.numbers);
      }
    })
    this.getallColor(0);
  }

  ngOnInit(): void {  }
  getallColor(pageno:number=0)
  {
    this.selectedIndex = pageno;
    let queryParam = {pageno:pageno};
    this.http.post(this.base_url + "getAllPickedUpByAdmin", queryParam).subscribe(res=>{
      this.ordersarray = res
      this.orders = this.ordersarray.data

      //console.log(this.orders)
    })
  }
  printPage(){
    window.print()
  }

  onPrintInvoice() {
    const invoiceIds = ['101', '102'];
    this.printService
      .printDocument('shipping_label', invoiceIds);
  }

  form = new UntypedFormGroup({
    customer_id: new UntypedFormControl('', []),
    form_date: new UntypedFormControl('', []),
    to_date: new UntypedFormControl('', []),
    delivery_status: new UntypedFormControl('', []),
    status: new UntypedFormControl('', []),
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  submit()
  {
    if(this.form.valid)
    {
      console.log(this.form.value);
      //this.Renderchart(this.form.value.form_date,this.form.value.to_date);
      // "delivery_status":"",
      // "status":""
      let queryParam = {"start_date":this.form.value.form_date,"end_date":this.form.value.to_date,"delivery_status":this.form.value.delivery_status,"status":this.form.value.status,"customer_id":this.form.value.customer_id};
      this.http.post(this.base_url+"getAllPickedUpByAdmin",queryParam).subscribe(res => {
        this.apiResponse = res;
        this.orders =this.apiResponse.data;
        console.log(this.orders);
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
  @HostListener('window:scroll', [])
  onScroll(): void 
  {
    const triggerAt: number = 100; 
    /* perform an event when the user has scrolled over the point of 128px from the bottom */
    if (document.body.scrollHeight - (window.innerHeight + window.scrollY) < triggerAt) {
      //doSomething();
      console.log("in bottom");
      
      if(this.totalPageNumber > this.pageStart)
      {
         
         

  
        //console.log(this.queryParam);
        console.log("this.api_response_var "+this.api_response_var)
        if(this.api_response_var == 0)
        {
          this.api_response_var = 1;


          let queryParam = {pageno:this.pageNo};
          this.http.post(this.base_url + "getAllPickedUpByAdmin", queryParam).subscribe(res=>{
            this.ordersarray = res;
            this.api_response_var = 0;
            if(this.ordersarray.status == true)
            {
              
              // this.allCarAdNew  .push(this.allCarAd);
              // console.log("this.allCarAd");

              // const fruits = ["Banana", "Orange", "Apple", "Mango"];
              // fruits.includes("Mango");
              
              // console.log(this.allCarAd);
              // console.log("this.allCarAdNew");
              // console.log(this.allCarAdNew);
              const fruits = this.ordersarray.data;
              fruits.map((object:any)=>{
                //console.log(object)
                this.orders.push(object);

              });
              console.log("this.allCarAdNew");
              console.log(this.orders);
              this.pageNo ++;
              //this.allCarAd = this.apiResponse.record;
              ///this.allTypeDePiece = this.apiResponse.record;
              //this.allCarAd = this.apiResponse.record;
            }
          });
        }
        


      } 
    }else{
      console.log("not in bottom");
    }
  }

  downloadOrderInvoice2(order_idd:any)
  {
    this.http.get(this.base_url_two+"api/event/createOrderInvoiceQrCode/"+order_idd).subscribe(res => {
      this.apiresponse = res
       if(this.apiresponse.status == true)
       {
          //window.location.href = this.apiresponse.url;
          window.open(this.apiresponse.url, 'download');  
       }
    })
  }
}
