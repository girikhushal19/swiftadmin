import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common'; 


@Component({
  selector: 'app-admin-client-detail',
  templateUrl: './admin-client-detail.component.html',
  styleUrls: ['./admin-client-detail.component.css']
})
export class AdminClientDetailComponent implements OnInit {
  order_id: string;
  base_url = "";base_url_node = ""; attributeArrayValue:any;
  token:any;formData:any;images:any;video:any;user_type:any;apiResponse:any;formValue:any;editRestaurantSubmit:any;getCategory:any;allModelList:any;getCarCategory:any;allCarCategory:any;getClientDetail:any;getqueryParam:any;
  base_url_node_only:any;record:any; 

  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
     

    this.order_id = this.actRoute.snapshot.params['id'];
    this.getqueryParam = {"order_id":this.order_id};
    this.getClientDetail = this.base_url_node+"getAdminClientDetail"; 

    this._http.post(this.getClientDetail,this.getqueryParam).subscribe((response:any)=>{
      
      this.record = response.record;
      console.log("this record "+this.record);
    });
  }
  ngOnInit(): void {
   
  }
  


 

}
